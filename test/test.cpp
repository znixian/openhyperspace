//
// Created by znix on 27/11/2021.
//

#include "test.h"

#include "demands/crew_demand.h"
#include "demands/demand.h"
#include "demands/room_demand.h"

#include "common/function_defs.h"
#include "linux/ftl_ship_structure.h"
#include "linux/sym_parser.h"
#include "linux/ftl_gui.h"
#include "test_unit.h"
#include "xml/hs_model.h"

#include "test_hook.h"

#include <subhook.h>

#include <dlfcn.h>
#include <fstream>
#include <stdio.h>

subhook::Hook* startGameHook = nullptr;
subhook::Hook* shipImportHook = nullptr;
subhook::Hook* mainHook = nullptr;

typedef int (*main_func_t)(int argc, char** argv);
extern "C" {
main_func_t mainFunc = nullptr;
}

struct ShipModificationSpec {
    std::vector<FtlRoom*> rooms;
};
static std::map<FtlShip*, ShipModificationSpec> shipModifications;

static float testTime = 0;

static void on_start_game_hook(FtlWorldManager* world, FtlShipManager* shipManager)
{
    subhook::ScopedHookRemove rem(startGameHook);
    fn.WorldManager_StartGame(world, shipManager);

    const FtlWeaponBlueprint* weapon = fn.BlueprintManager_GetWeaponBlueprint(fn.BlueprintManager_Instance, "BOMB_FIRE");
    // Note about the 'free' parameter - this is copied from ModifyResources which set it to true, haven't figured out exactly
    // what it does yet.
    fn.Equipment_AddWeapon(&ftlApp->world->commandGui->equipment, weapon, true, false);

    weapon = fn.BlueprintManager_GetWeaponBlueprint(fn.BlueprintManager_Instance, "BOMB_1");
    // Note about the 'free' parameter - this is copied from ModifyResources which set it to true, haven't figured out exactly
    // what it does yet.
    fn.Equipment_AddWeapon(&ftlApp->world->commandGui->equipment, weapon, true, false);

    FtlCrewBlueprint bp;
    fn.BlueprintManager_GetCrewBlueprint(&bp, fn.BlueprintManager_Instance, FtlString("energy"));
    bp.crewNameLong.str = "test";
    bp.crewNameLong.isLiteral = true;

    fn.BlueprintManager_GetCrewBlueprint(&bp, fn.BlueprintManager_Instance, FtlString("engi"));
    bp.crewNameLong.str = "hello";
    FtlCrewMember* crew2 = fn.CrewMemberFactory_CreateCrewmember(fn.CrewMemberFactory_Instance, &bp, 0, false);
    fn.CompleteShip_AddCrewMember_ByObject(ftlApp->world->playerShip, crew2, 0);

    // OldString name;
    // fn.CompleteShip_AddCrewMember_ByString(ftlApp->world->playerShip, "ghost", &name, false);
    // fn.CompleteShip_AddCrewMember_ByString(ftlApp->world->playerShip, "anaerobic", &name, false);
    // printf("created crew with name %s\n", name.c_str());
}

static bool on_ship_import(FtlShip* this_, const FtlString& name)
{
    subhook::ScopedHookRemove shr(shipImportHook);
    bool success = fn.Ship_Import(this_, name);

    if (!success)
        return success;

    // Find the required set of modifications
    auto iter = shipModifications.find(this_);
    if (iter == shipModifications.end())
        return success;

    ShipModificationSpec spec = std::move(shipModifications.at(this_));
    shipModifications.erase(this_);

    for (FtlRoom* room : spec.rooms) {
        // Note: roomList must contain rooms with IDs 0..len-1 so if we make up or reuse an ID here it'll break the positioning
        // See ShipGraph::ComputeCenter for the relevant code.
        room->roomId = this_->roomList.size();
        this_->roomList.push_back(room);
    }

    return success;
}

static void on_startup()
{
    const FtlShipBlueprint* shipBlueprint = fn.BlueprintManager_GetShipBlueprint(fn.BlueprintManager_Instance, "PLAYER_SHIP_HARD", -1);

    auto* shipManager = (FtlShipManager*)calloc(1, sizeof(FtlShipManager));
    fn.ShipManager_Ctor(shipManager, 0);

    // Create all the required rooms - must be done before OnInit
    // While we're here, also collect all the various types of demands
    ShipModificationSpec& spec = shipModifications[&shipManager->ship];
    std::vector<CrewDemand*> crewDemands;
    int x = 0;
    for (TestUnit* test : TestUnit::testsList) {
        test->shipManager = shipManager;

        for (Demand* demand : test->Demands()) {
            if (&typeid(*demand) == &typeid(CrewDemand))
                crewDemands.push_back((CrewDemand*)demand);

            if (&typeid(*demand) != &typeid(RoomDemand))
                continue;
            auto* rd = (RoomDemand*)demand;

            auto* room = (FtlRoom*)calloc(1, sizeof(FtlRoom));
            fn.Room_Ctor(room, 0, x++, 8, 1, 2, -1); // Room ID will be overwritten later, during the ship setup
            rd->room = room;
            spec.rooms.push_back(room);
        }
    }

    fn.ShipManager_OnInit(shipManager, shipBlueprint, 0);

    // Now spawn the crew members
    int i = 0; // Get a deterministic name ordering
    for (CrewDemand* demand : crewDemands) {
        demand->Spawn(shipManager, i);
    }

    fn.WorldManager_StartGame(ftlApp->world, shipManager);

    for (TestUnit* test : TestUnit::testsList) {
        test->OnLoad();
    }

    fn.MainMenu_Close((FtlMainMenu*)ftlApp->_main_menu);
}

static void on_update()
{
    float speedFactor = fn.CFPS_GetSpeedFactor(fn.CFPS_FPSControl);
    testTime += speedFactor;
    for (TestUnit* test : TestUnit::testsList) {
        test->testTime = testTime;
        test->speedFactor = speedFactor;
        test->OnFrame();
    }
}

// Set maybe_unused since GCC and Clang can't figure out this is used by the inline assembly
extern "C" [[maybe_unused]] void test_main()
{
    // Get rid of the main function hook, so when we return back the game will run
    assert(mainHook->Remove());

    printf("test main was run!\n");

    bool success;

    startGameHook = new subhook::Hook((void*)fn.WorldManager_StartGame, (void*)on_start_game_hook, subhook::HookFlag64BitOffset);
    success = startGameHook->Install();
    if (!success) {
        abort();
    }

    shipImportHook = new subhook::Hook((void*)fn.Ship_Import, (void*)on_ship_import, subhook::HookFlag64BitOffset);
    success = shipImportHook->Install();
    if (!success) {
        abort();
    }

    // Find the path of our .so file so we can inspect it
    CBString testLibraryPath;

    std::ifstream maps;
    maps.exceptions(std::ios::badbit);
    maps.open("/proc/self/maps");
    while (!maps.eof()) {
        CBString line;
        getline(maps, line);

        line.trim();
        if (line.length() == 0)
            continue;

        // Cut off everything before the last space - this leaves us with the filename only
        int spaceIdx = line.reversefind(' ', line.length());
        if (spaceIdx == -1)
            continue;
        line.remove(0, spaceIdx + 1);

        CBString suffix = "/libhstest.so";
        if (line.find(suffix) != line.length() - suffix.length())
            continue;

        testLibraryPath = line;
        printf("Found test library at '%s'\n", (const char*)line);
    }

    if (testLibraryPath.length() == 0) {
        fprintf(stderr, "Failed to find libhstest.so path\n");
        abort();
    }

    void* handle = dlopen(nullptr, RTLD_LAZY);
    assert(handle);

    SymbolSet symbols(testLibraryPath);
    for (const auto& pair : symbols.symbols) {
        CBString symbolName = pair.first.c_str();
        if (symbolName.find("TESTUNIT_REGISTER_") == -1)
            continue;

        auto* func = (void (*)())dlsym(handle, pair.first.c_str());
        func();
    }

    dlclose(handle);

    printf("Tests available:\n");
    for (TestUnit* test : TestUnit::testsList) {
        printf("\t%s\n", (const char*)test->name);
    }
}

// Kinda nasty hack to run our hook function before main
// We could hook main, but this lets us avoid pulling in another symbol
// Note: We must preserve RDX, see https://www.intel.com/content/dam/develop/external/us/en/documents/mpx-linux64-abi.pdf

extern "C" void hstest_on_main();
__asm__(
    ".globl hstest_on_main\n"
    "hstest_on_main:"
    "\n push %rdx"
    "\n call test_main"
    "\n mov mainFunc@GOTPCREL(%rip), %rdx"
    "\n movq 0(%rdx), %rcx"
    "\n pop %rdx"
    "\n call *%rcx"
    "\n");

extern "C" [[maybe_unused]] void openhs_test_main(TestHooks* hooks)
{
    hooks->onStartup = on_startup;
    hooks->onUpdate = on_update;

    // This will totally break with ASLR, but it's fine until then
    mainFunc = (main_func_t)hooks->entry;

    // Run our main code after all the DLL loading is done, otherwise the variables (except those set to zero and in BSS) will be re-initialised.
    mainHook = new subhook::Hook((void*)mainFunc, (void*)hstest_on_main, subhook::HookFlag64BitOffset);
    bool success = mainHook->Install();
    if (!success) {
        abort();
    }
}
