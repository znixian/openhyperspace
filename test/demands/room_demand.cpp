//
// Created by znix on 04/12/2021.
//

#include "room_demand.h"
#include "crew_demand.h"

void RoomDemand::Reset()
{
    Demand::Reset();

    room = nullptr;
}

int RoomDemand::Id() const
{
    return room->roomId;
}
