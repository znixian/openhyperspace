//
// Created by znix on 04/12/2021.
//

#pragma once

#include "demand.h"

class RoomDemand : public Demand {
public:
    using Demand::Demand;

    /**
     * Once this demand has been satisfied, this will be set to the created room.
     */
    class FtlRoom* room = nullptr;

    void Reset() override;

    /**
     * Get the room ID.
     */
    [[nodiscard]] int Id() const;
};
