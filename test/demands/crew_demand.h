//
// Created by znix on 05/12/2021.
//

#pragma once

#include "demand.h"
#include "room_demand.h"

#include <bstrwrap.h>
#include <linux/ftl_ship_structure.h>

class CrewDemand : public Demand {
public:
    CrewDemand(TestUnit* unit, const CBString& race, const RoomDemand* room, bool intruder = false);

    void Reset() override;

    void Spawn(FtlShipManager* shipManager, int seqNo);

private:
    CBString race;
    const RoomDemand* room;
    bool intruder;

    struct FtlCrewMember* crewMember = nullptr;
};
