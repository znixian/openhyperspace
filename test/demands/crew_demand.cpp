//
// Created by znix on 05/12/2021.
//

#include "crew_demand.h"

#include <linux/ftl_ship_structure.h>
#include <test_unit.h>

CrewDemand::CrewDemand(TestUnit* unit, const CBString& race, const RoomDemand* room, bool intruder)
    : Demand(unit)
    , race(race)
    , room(room)
    , intruder(intruder)
{
}

void CrewDemand::Reset()
{
    crewMember = nullptr;
}

void CrewDemand::Spawn(FtlShipManager* shipManager, int seqNo)
{
    CBString defaultName;
    defaultName.format("%s_%d", (const char*)test->name, seqNo);
    OldString name(defaultName);
    OldString ftlRace(race);
    FtlRoom *realRoom = room->room;
    crewMember = fn.ShipManager_AddCrewMember_ByString(shipManager, &name, &ftlRace, intruder, realRoom->roomId, true, true);
}
