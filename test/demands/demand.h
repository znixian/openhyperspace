//
// Created by znix on 04/12/2021.
//

#pragma once

/**
 * Demand is an abstract superclass for any kind of environment requirement the test has.
 *
 * Since stuff like ship setup has to be done once before we let a batch of tests run, and adding rooms has to be
 * done at a specific time and we probably don't want them overlapping so it would be complicated to do in an
 * imperative fashion, with each test adding rooms etc during certain times.
 *
 * Rather if the tests declare their requirements, we can make an optimal set of test ships to test everything
 * as fast as possible.
 */
class Demand {
public:
    explicit Demand(class TestUnit* test);

    Demand(const Demand&) = delete;
    Demand* operator=(const Demand&) = delete;

    virtual ~Demand() = default;

    /**
     * Resetting a demand will clear it back to the default state.
     *
     * This is done whenever the ship replaced, as it makes sure if the test is run twice previous values can't
     * get left over and contaminate the next test.
     */
    virtual void Reset() { }

protected:
    class TestUnit *test = nullptr;
};
