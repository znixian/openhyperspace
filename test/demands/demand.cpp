//
// Created by znix on 04/12/2021.
//

#include "demand.h"

#include "test_unit.h"

Demand::Demand(TestUnit* test)
    : test(test)
{
    test->RegisterDemand(this);
}
