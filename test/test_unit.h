//
// Created by znix on 27/11/2021.
//

#pragma once

#include "demands/demand.h"

// We don't need these includes, but save typing for the tests
#include "demands/crew_demand.h"
#include "demands/room_demand.h"

#include <memory>
#include <set>
#include <vector>

#include <bstrwrap.h>

// Export a symbol that the test framework will detect
#define REGISTER_TEST(ClassName)                    \
    extern "C" void TESTUNIT_REGISTER_##ClassName() \
    {                                               \
        auto* cls = new ClassName();                \
        cls->name = #ClassName;                     \
        TestUnit::testsList.push_back(cls);         \
    }

#define HS_ASSERT(expr)                                                    \
    do {                                                                   \
        static int hsMacro_assertionId = NextAssertionId();                \
        int result = expr;                                                 \
        if (result == 0) {                                                 \
            FailAssertion(hsMacro_assertionId, __func__, __LINE__, #expr); \
        }                                                                  \
    } while (0)

class TestUnit {
public:
    static std::vector<TestUnit*> testsList;

    CBString name;

    virtual ~TestUnit() = default;

    /**
     * Run when the game is started.
     */
    virtual void OnLoad()
    {
        hasLoaded = true;
    }

    /**
     * Called once per frame.
     */
    virtual void OnFrame() { }

    /**
     * Get this test's demands.
     */
    [[nodiscard]] inline const std::vector<Demand*>& Demands() const { return demands; }

    /**
     * The ship we're currently running on in this test.
     */
    FtlShipManager* shipManager = nullptr;

    /**
     * The time that the test has been running for, in CFPS units.
     */
    float testTime = 0;

    /**
     * The time since the last tick, as returned by CFPS.
     */
    float speedFactor = 0;

protected:
    void FailAssertion(int assertionId, const char* function, int line, const char* expr);
    static int NextAssertionId();

    bool hasLoaded = false;
    std::vector<Demand*> demands;

private:
    static int nextAssertionId;
    std::set<int> failedAssertions;

    void RegisterDemand(Demand* demand);
    friend Demand::Demand(TestUnit*);
};
