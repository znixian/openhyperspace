//
// Created by znix on 27/11/2021.
//

#include "test_unit.h"

#include "linux/ftl_systems.h"

class AnaerobicCrewTest : public TestUnit {
public:
    RoomDemand room = RoomDemand(this);
    CrewDemand crew = CrewDemand(this, "anaerobic", &room);

    void OnLoad() override
    {
        TestUnit::OnLoad();
        shipManager->oxygenSystem->SetOxygenLevel(room.Id(), 100);
    }

    float lastReal = 100;
    float lastTarget = 100;

    float firstFrameTime = 0;

    void OnFrame() override
    {
        // Ignore very early on during spawning
        if (testTime < 0.001)
            return;

        if (firstFrameTime == 0)
            firstFrameTime = testTime;

        // The first time the oxygen was updated was before the crewmember spawned, so account for that
        float oxygenRefillRate = 0.075; // Correct for a level-1 powered oxygen system
        float oxygenEffect = oxygenRefillRate * (testTime - firstFrameTime);

        float drainSpeed = 0.525; // From oxygenChangeSpeed in hyperspace.xml
        float requiredAmount = 100 - drainSpeed * testTime + oxygenEffect;
        if (requiredAmount < 0)
            requiredAmount = 0;

        // The real amount fluctuates below zero because the rounding is done later on
        float realAmount = shipManager->oxygenSystem->GetOxygenLevel(room.Id());
        if (realAmount < 0)
            realAmount = 0;

        float delta = abs(realAmount - requiredAmount);

        float deltaReal = realAmount - lastReal;
        float deltaTarget = requiredAmount - lastTarget;
        lastReal = realAmount;
        lastTarget = requiredAmount;

        // Block warnings when the printf is commented out
        (void)deltaReal;
        (void)deltaTarget;

        // printf("real %.4f   target %.4f - Δr %.4f   Δt %.4f   time %.4f\n", realAmount, requiredAmount, deltaReal, deltaTarget, testTime);
        HS_ASSERT(delta < 0.01);
    }
};

REGISTER_TEST(AnaerobicCrewTest)
