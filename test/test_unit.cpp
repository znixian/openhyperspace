//
// Created by znix on 27/11/2021.
//

#include "test_unit.h"

int TestUnit::nextAssertionId = 10001;
std::vector<TestUnit*> TestUnit::testsList;

int TestUnit::NextAssertionId()
{
    return nextAssertionId++;
}

void TestUnit::FailAssertion(int assertionId, const char* function, int line, const char* expr)
{
    if (failedAssertions.count(assertionId))
        return;
    failedAssertions.insert(assertionId);

    fprintf(stderr, "[%s] Assertion Failed! At %s:%d\n", (const char*)name, function, line);
}

void TestUnit::RegisterDemand(Demand* demand)
{
    if (hasLoaded) {
        fprintf(stderr, "Cannot call RegisterDemand after OnLoad");
        abort();
    }
    demands.push_back(demand);
}
