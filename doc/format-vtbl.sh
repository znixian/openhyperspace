#!/bin/bash
exec sed -nE 's/.*  addr +(.*)/\1/p' | awk '{offset=(NR-1)*8; printf "%4d  0x%03x  %2d 0x%02x   %s\n", offset, offset, NR-1, NR-1, $0}'
