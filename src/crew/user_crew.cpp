//
// Created by znix on 14/11/2021.
//

#include "user_crew.h"

UserCrewMember::UserCrewMember(const CrewTypeDefinition* type, int shipId, bool intruder, FtlCrewAnimation* anim)
    : UserCrewMember(type, type->FindBlueprint(), shipId, intruder, anim)
{
}

UserCrewMember::UserCrewMember(const CrewTypeDefinition* type, const FtlCrewBlueprint& bp, int shipId, bool intruder, FtlCrewAnimation* anim)
    : BaseCrewMember(bp, shipId, intruder, anim)
    , type(type)
{
    // Start fully healed, whatever our health
    health.first = type->maxHealth;
}

bool UserCrewMember::GetControllable()
{
    return type->controllable;
}
bool UserCrewMember::CanFight()
{
    return type->canFight;
}
bool UserCrewMember::CanRepair()
{
    return type->canRepair;
}
bool UserCrewMember::CanSabotage()
{
    return type->canSabotage;
}
bool UserCrewMember::CanMan()
{
    return type->canMan;
}
bool UserCrewMember::CanTeleport()
{
    // TODO implement?
    return BaseCrewMember::CanTeleport();
}
bool UserCrewMember::CanHeal()
{
    // TODO implement?
    return BaseCrewMember::CanHeal();
}
bool UserCrewMember::CanSuffocate()
{
    return type->canSuffocate;
}
bool UserCrewMember::CanBurn()
{
    return type->canBurn;
}
int UserCrewMember::GetMaxHealth()
{
    return type->maxHealth;
}
bool UserCrewMember::ProvidesVision()
{
    // TODO implement?
    return BaseCrewMember::ProvidesVision();
}
float UserCrewMember::GetMoveSpeedMultiplier()
{
    return type->moveSpeedMultiplier;
}
float UserCrewMember::GetRepairSpeed()
{
    return type->repairSpeed;
}
float UserCrewMember::GetDamageMultiplier()
{
    return type->damageMultiplier;
}
bool UserCrewMember::ProvidesPower()
{
    return type->bonusPower != 0;
}
float UserCrewMember::GetFireRepairMultiplier()
{
    return type->fireRepairMultiplier;
}
bool UserCrewMember::IsTelepathic()
{
    return type->isTelepathic;
}
float UserCrewMember::GetSuffocationModifier()
{
    return type->suffocationModifier;
}
FtlDamage UserCrewMember::GetRoomDamage()
{
    // TODO implement?
    return BaseCrewMember::GetRoomDamage();
}
bool UserCrewMember::IsAnaerobic()
{
    return type->isAnaerobic;
}

void UserCrewMember::HookUpdateHealth()
{
    float deltaTime = fn.CFPS_GetSpeedFactor(fn.CFPS_FPSControl);

    // This is the bit we care about - applying the burn multiplier is why we have this hook at all
    if (onFire && CanBurn()) {
        fn.CrewMember_DirectModifyHealth(this, (float)onFire * deltaTime * -0.133f * type->fireDamageMultiplier);
    }

    if (suffocating && !IsAnaerobic()) {
        float maskDamageMult = 1.0f;

        // TODO test the oxygen masks code
        FtlString tmpName("O2_MASKS");
        if (fn.ShipObject_HasEquipment(this, &tmpName)) {
            maskDamageMult = fn.ShipObject_GetAugmentationValue(this, &tmpName);
        }

        fn.CrewMember_DirectModifyHealth(this, deltaTime * maskDamageMult * -0.4f * GetSuffocationModifier());
    }

    // The real code has a weird check for CrewMember::CanHeal if it's an anaerobic, non-suffocating crewmember, but it
    // ignores the return result. Since SetMedbay checks CanHeal anyway, IMO this is probably just loading a variable that's
    // used by commented-out code. Since it's a virtual call the compiler wouldn't be able to safely optimise it away since
    // it could have unknown side-effects, so it remains there.

    float medbaySpeed = medbay;

    // This looks weird, copied directly from the decompiled code
    if (1.0 < medbaySpeed) {
        medbaySpeed = (medbaySpeed - 1.0f) * 1.5f;
    }

    float droneFactor = IsDrone() ? 0.2f : 1.0f;

    fn.CrewMember_DirectModifyHealth(this, deltaTime * medbaySpeed * 0.4f * droneFactor);
}
