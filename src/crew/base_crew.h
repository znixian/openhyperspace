//
// Created by znix on 13/11/2021.
//

#pragma once

#include "linux/ftl_ship_objects.h"

/**
 * A version of FtlCrewMember with all the functions either delegating to their base-game versions, or
 * reimplemented in-mod for simple functions.
 */
class BaseCrewMember : public FtlCrewMember {
public:
    using FtlCrewMember::FtlCrewMember;
    ~BaseCrewMember() override;

    // From FtlCrewTarget
    FtlPoint GetPosition() override;
    float PositionShift() override;
    bool InsideRoom(int roomId) override;
    bool ApplyDamage(float damage) override;
    int GetPriority() override;
    bool ValidTarget(int shipId) override;
    bool MultiShots() override;
    bool ExactTarget() override;
    bool IsCrew() override;
    bool IsCloned() override;
    bool IsDrone() override;

    // From FtlCrewMember
    void Jump() override;
    bool GetIntruder() override;
    void SaveState(int fd) override;
    void LoadState(int fd) override;
    void OnLoop() override;
    void OnRender(bool outlineOnly) override;
    bool OutOfGame() override;
    void SetOutOfGame() override;
    bool Functional() override;
    bool CountForVictory() override;
    bool GetControllable() override;
    bool ReadyToFight() override;
    bool CanFight() override;
    bool CanRepair() override;
    bool CanSabotage() override;
    bool CanMan() override;
    bool CanTeleport() override;
    bool CanHeal() override;
    bool CanSuffocate() override;
    bool CanBurn() override;
    int GetMaxHealth() override;
    bool IsDead() override;
    bool PermanentDeath() override;
    bool ShipDamage(float amount) override;
    bool FireFightingSoundEffect() override;
    FtlString GetUniqueRepairing() override;
    bool ProvidesVision() override;
    float GetMoveSpeedMultiplier() override;
    float GetRepairSpeed() override;
    float GetDamageMultiplier() override;
    bool ProvidesPower() override;
    FtlString GetSpecies() override;
    float GetFireRepairMultiplier() override;
    bool IsTelepathic() override;
    void GetPowerCooldown() override;
    bool PowerReady() override;
    void ActivatePower() override;
    bool HasSpecialPower() override;
    void ResetPower() override;
    float GetSuffocationModifier() override;
    int BlockRoom() override;
    FtlDamage GetRoomDamage() override;
    bool IsAnaerobic() override;
    void UpdateRepair() override;
    bool CanStim() override;
};
