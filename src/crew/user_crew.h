//
// Created by znix on 14/11/2021.
//

#pragma once

#include "base_crew.h"

#include "xml/hs_model.h"

/**
 * A user-defined (via XML) crewmember.
 *
 * See CrewTypeDefinition for more information.
 */
class UserCrewMember : public BaseCrewMember {
public:
    UserCrewMember(const CrewTypeDefinition* type, int shipId, bool intruder, FtlCrewAnimation* anim);
    UserCrewMember(const CrewTypeDefinition* type, const FtlCrewBlueprint& bp, int shipId, bool intruder, FtlCrewAnimation* anim);

    [[nodiscard]] inline const CrewTypeDefinition* GetUserType() const { return type; }

    // Overrides

    bool GetControllable() override;
    bool CanFight() override;
    bool CanRepair() override;
    bool CanSabotage() override;
    bool CanMan() override;
    bool CanTeleport() override;
    bool CanHeal() override;
    bool CanSuffocate() override;
    bool CanBurn() override;
    int GetMaxHealth() override;
    bool ProvidesVision() override;
    float GetMoveSpeedMultiplier() override;
    float GetRepairSpeed() override;
    float GetDamageMultiplier() override;
    bool ProvidesPower() override;
    float GetFireRepairMultiplier() override;
    bool IsTelepathic() override;
    float GetSuffocationModifier() override;
    FtlDamage GetRoomDamage() override;
    bool IsAnaerobic() override;

    void HookUpdateHealth();

private:
    const CrewTypeDefinition* type;
};
