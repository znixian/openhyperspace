//
// Created by znix on 13/11/2021.
//

#include "base_crew.h"

BaseCrewMember::~BaseCrewMember()
{
}

FtlPoint BaseCrewMember::GetPosition()
{
    return FtlPoint((int)x, (int)y);
}
float BaseCrewMember::PositionShift()
{
    // TODO
    return 0;
}
bool BaseCrewMember::InsideRoom(int roomId)
{
    return this->roomId == roomId;
}
bool BaseCrewMember::ApplyDamage(float amount)
{
    float prevHealth = this->health.first;
    fn.CrewMember_DirectModifyHealth(this, amount);
    return prevHealth > 0.0 && this->health.first <= 0.0;
}
int BaseCrewMember::GetPriority()
{
    // Magic value from CrewMember, copied in here
    return 100;
}
bool BaseCrewMember::ValidTarget(int shipId)
{
    if (shipId != currentShipId)
        return false;
    if (IsDeadOrDying())
        return false;
    return true;
}
bool BaseCrewMember::MultiShots()
{
    return false;
}
bool BaseCrewMember::ExactTarget()
{
    return false;
}
bool BaseCrewMember::IsCrew()
{
    return !IsDrone();
}
bool BaseCrewMember::IsCloned()
{
    return cloned;
}
bool BaseCrewMember::IsDrone()
{
    return false;
}

// Start of CrewMember fields
void BaseCrewMember::Jump()
{
    // TODO
}
bool BaseCrewMember::GetIntruder()
{
    if (mindControlled)
        return currentShipId == shipId;
    else
        return currentShipId != shipId;
}
void BaseCrewMember::SaveState(int fd)
{
    fn.CrewMember_SaveState(this, fd);
}
void BaseCrewMember::LoadState(int fd)
{
    fn.CrewMember_LoadState(this, fd);
}
void BaseCrewMember::OnLoop()
{
    fn.CrewMember_OnLoop(this);
}
void BaseCrewMember::OnRender(bool outlineOnly)
{
    fn.CrewMember_OnRender(this, outlineOnly);
}
bool BaseCrewMember::OutOfGame()
{
    return outOfGame;
}
void BaseCrewMember::SetOutOfGame()
{
    fn.CrewMember_SetOutOfGame(this);
}
bool BaseCrewMember::Functional()
{
    return !IsDead();
}
bool BaseCrewMember::CountForVictory()
{
    if (cloneReady)
        return true;
    return !IsDeadOrDying();
}
bool BaseCrewMember::GetControllable()
{
    if (shipId != 0)
        return false;
    return !IsDead() && !mindControlled;
}
bool BaseCrewMember::ReadyToFight()
{
    float dx = this->x - (float)this->currentSlot.worldLocation.x;
    float dy = this->y - (float)this->currentSlot.worldLocation.y;
    return abs(dx) < 0.5 && abs(dy) < 0.5;
}
bool BaseCrewMember::CanFight()
{
    return true;
}
bool BaseCrewMember::CanRepair()
{
    return !IsDeadOrDying();
}
bool BaseCrewMember::CanSabotage()
{
    return intruder;
}
bool BaseCrewMember::CanMan()
{
    if (intruder || stunTime > 0)
        return false;
    return !IsDeadOrDying();
}
bool BaseCrewMember::CanTeleport()
{
    if (IsDeadOrDying())
        return false;

    // Decompiled and cleaned up from the base game
    float dx = this->x - (float)(this->currentSlot).worldLocation.x;
    float dy = this->y - (float)(this->currentSlot).worldLocation.y;
    bool atSlot = abs(dx) <= 0.5 && abs(dy) <= 0.5;
    if (!atSlot && this->currentSlot.roomId != this->roomId && !GetIntruder()) {
        return false;
    }
    if (shipId != 0) {
        return true;
    }
    return !mindControlled;
}
bool BaseCrewMember::CanHeal()
{
    // Cleaned up and decompiled from game

    if (this->health.first != this->health.second)
        return true;

    if (suffocating && !IsAnaerobic())
        return true;

    if (onFire >= 1)
        return true;

    if (fighting)
        return this->crewTarget != this->blockingDoor;

    return false;
}
bool BaseCrewMember::CanSuffocate()
{
    return true;
}
bool BaseCrewMember::CanBurn()
{
    return true;
}
int BaseCrewMember::GetMaxHealth()
{
    return 100;
}
bool BaseCrewMember::IsDead()
{
    return dead;
}
bool BaseCrewMember::PermanentDeath()
{
    return true;
}
bool BaseCrewMember::ShipDamage(float amount)
{
    return fn.CrewMember_ShipDamage(this, amount);
}
bool BaseCrewMember::FireFightingSoundEffect()
{
    if (stunTime > 0)
        return false;
    return RepairingFire();
}
FtlString BaseCrewMember::GetUniqueRepairing()
{
    return FtlString("store_title_repair");
}
bool BaseCrewMember::ProvidesVision()
{
    return shipId == 0 || mindControlled;
}
float BaseCrewMember::GetMoveSpeedMultiplier()
{
    return 1;
}
float BaseCrewMember::GetRepairSpeed()
{
    return 1;
}
float BaseCrewMember::GetDamageMultiplier()
{
    return 1;
}
bool BaseCrewMember::ProvidesPower()
{
    return false;
}
FtlString BaseCrewMember::GetSpecies()
{
    return species;
}
float BaseCrewMember::GetFireRepairMultiplier()
{
    // Yes, this is the default
    return 1.2;
}
bool BaseCrewMember::IsTelepathic()
{
    return false;
}
void BaseCrewMember::GetPowerCooldown()
{
    // Oh no, this stupid thing
    // TODO implement and figure out how to make the ABI work
}
bool BaseCrewMember::PowerReady()
{
    return false;
}
void BaseCrewMember::ActivatePower()
{
}
bool BaseCrewMember::HasSpecialPower()
{
    return false;
}
void BaseCrewMember::ResetPower()
{
}
float BaseCrewMember::GetSuffocationModifier()
{
    return 1;
}
int BaseCrewMember::BlockRoom()
{
    // What?
    return -1;
}
FtlDamage BaseCrewMember::GetRoomDamage()
{
    return FtlDamage {
        .damage = 0,
        .shieldPiercing = 0,
        .fireChance = 0,
        .breachChance = 0,
        .stunChance = 0,
        .ionDamage = 0,
        .systemDamage = 0,
        .persDamage = 0,
        .hullBuster = false,
        .ownerId = -1,
        .selfId = -1,
        .lockdown = false,
        .crystalShard = false,
        .friendlyFire = true,
        .stun = 0,
    };
}
bool BaseCrewMember::IsAnaerobic()
{
    return false;
}
void BaseCrewMember::UpdateRepair()
{
    fn.CrewMember_UpdateRepair(this);
}
bool BaseCrewMember::CanStim()
{
    return true;
}
