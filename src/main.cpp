#include "crew/base_crew.h"
#include "crew/user_crew.h"

#include "xml/hs_model.h"

#include "linux/ftl_resource.h"
#include "linux/ftl_ship_structure.h"
#include "linux/ftl_structures.h"
#include "linux/ftl_weapons.h"
#include "linux/linux_functions.h"
#include "linux/mem_map.h"
#include "linux/sym_parser.h"

#include "common/function_defs.h"

#include "subhook.h"
#include "test_hook.h"

#include <cstdlib>
#include <dlfcn.h>
#include <memory>

static subhook::Hook* hook;
static subhook::Hook* executeHook;
static subhook::Hook* doneLoadHook;
static subhook::Hook* createCrewHook;
static subhook::Hook* updateEnvironmentHook;
static subhook::Hook* updateCrewHealthHook;
static subhook::Hook* updateWorldHook;

static std::unique_ptr<HsModel> hsModel;

static TestHooks testHooks {};

static void render_hook(FtlMainMenu* this_)
{
    subhook::ScopedHookRemove shi(hook);
    fn.MainMenu_OnRender(this_);

    //GL_Texture* background = *(GL_Texture**)((uint64_t)this_ + 8);
    //fn.CSurface_DisableBlend();
    //fn.ResourceControl_RenderImage_GL(*fn.ResourceControl_GlobalResources, background, 0, 0, 0, 1.0, false);
    //fn.CSurface_EnableBlend();

    //fn.ShipBuilder_Open((void*)((uint64_t)this_ + 1096));
    //*(bool*)((uint64_t)this_ + 1096 + 4636) = true;
}

static void on_execute_hook(FtlCApp* app)
{
    ftlApp = app;
    subhook::ScopedHookRemove rem(executeHook);
    fn.CApp_OnExecute(app);
}

static FtlCommandGui* on_create_gui_hook(FtlWorldManager* world)
{
    subhook::ScopedHookRemove rem(doneLoadHook);
    FtlCommandGui* gui = fn.WorldManager_CreateCommandGui(world);

    // Do any of our other on-startup loading stuff here
    FtlResourceControl* resCtl = fn.ResourceControl_GlobalResources;
    char* data = fn.ResourceControl_LoadFile(resCtl, "data/hyperspace.xml");
    hsModel = std::make_unique<HsModel>(data);

    printf("Loading done\n");

    if (testHooks.onStartup) {
        testHooks.onStartup();
    }

    return gui;
}

static FtlCrewMember* on_create_crewmember(FtlCrewMemberFactory* this_, FtlCrewBlueprint* blueprint, int shipId, bool intruder)
{
    printf("spawn crew '%s'\n", blueprint->name.c_str());
    const CrewTypeDefinition* def = hsModel->GetCrewType(blueprint->name.cxx_str());

    auto* animation = new FtlCrewAnimation(shipId, blueprint->name, FtlPointf(0, 0), false);
    FtlCrewMember* crew = new UserCrewMember(def, *blueprint, shipId, false, animation);
    fn.CrewMemberFactory_Instance->crewMembers.push_back(crew);

    return crew;
}

static void on_update_environment(FtlShipManager* this_)
{
    subhook::ScopedHookRemove shr(updateEnvironmentHook);
    fn.ShipManager_UpdateEnvironment(this_);

    for (int crewId = 0; crewId < this_->crewList.size(); crewId++) {
        FtlCrewMember* crew = this_->crewList[crewId];

        // Just assume RTTI works here, and fix it on Windows if they turned it off
        // FWIW on the old stripped binaries they still had RTTI compiled in
        if (&typeid(*crew) != &typeid(UserCrewMember)) {
            continue;
        }

        auto* userCrew = (UserCrewMember*)crew;
        float oxygenSpeed = userCrew->GetUserType()->oxygenChangeSpeed;
        if (abs(oxygenSpeed) > 0.00001) { // Irrational fear of tiny floating-point numbers
            // FIXME As an optimisation, FTL doesn't bother computing air loss for drained rooms. If the user enters a
            //  positive number for this (that is, add air back into the room) then it won't work if the room is
            //  completely empty of air. Also we may end up filling rooms over 100% oxygen, need to check. We should
            //  probably make our own version of this function.
            fn.OxygenSystem_ComputeAirLoss(this_->oxygenSystem, crew->roomId, -oxygenSpeed, true);
        }
    }
}

static void on_update_world(FtlWorldManager* world)
{
    subhook::ScopedHookRemove shr(updateWorldHook);
    fn.WorldManager_OnLoop(world);

    if (testHooks.onUpdate) {
        testHooks.onUpdate();
    }
}

static void on_update_crew_health(FtlCrewMember* this_)
{
    if (&typeid(*this_) == &typeid(UserCrewMember)) {
        ((UserCrewMember*)this_)->HookUpdateHealth();
    } else {
        subhook::ScopedHookRemove shr(updateCrewHealthHook);
        return fn.CrewMember_UpdateHealth(this_);
    }
}

__attribute__((constructor)) static void setup()
{
    MemMaps maps;

    // If there's no maps, we're not running FTL
    if (maps.regions.empty())
        return;

    // Get the first start_region, which is mapped to 0 - this contains the ELF header
    const MemMaps::Region* startRegion = maps.translate(0);

    SymbolSet symbols(startRegion->path);

    linux_load_functions(&fn, &symbols);

    hook = new subhook::Hook((void*)fn.MainMenu_OnRender, (void*)render_hook, subhook::HookFlag64BitOffset);
    bool success = hook->Install();
    if (!success) {
        abort();
    }

    executeHook = new subhook::Hook((void*)fn.CApp_OnExecute, (void*)on_execute_hook, subhook::HookFlag64BitOffset);
    success = executeHook->Install();
    if (!success) {
        abort();
    }

    doneLoadHook = new subhook::Hook((void*)fn.WorldManager_CreateCommandGui, (void*)on_create_gui_hook, subhook::HookFlag64BitOffset);
    success = doneLoadHook->Install();
    if (!success) {
        abort();
    }

    createCrewHook = new subhook::Hook((void*)fn.CrewMemberFactory_CreateCrewmember, (void*)on_create_crewmember, subhook::HookFlag64BitOffset);
    success = createCrewHook->Install();
    if (!success) {
        abort();
    }

    updateEnvironmentHook = new subhook::Hook((void*)fn.ShipManager_UpdateEnvironment, (void*)on_update_environment, subhook::HookFlag64BitOffset);
    success = updateEnvironmentHook->Install();
    if (!success) {
        abort();
    }

    updateCrewHealthHook = new subhook::Hook((void*)fn.CrewMember_UpdateHealth, (void*)on_update_crew_health, subhook::HookFlag64BitOffset);
    success = updateCrewHealthHook->Install();
    if (!success) {
        abort();
    }

    updateWorldHook = new subhook::Hook((void*)fn.WorldManager_OnLoop, (void*)on_update_world, subhook::HookFlag64BitOffset);
    success = updateWorldHook->Install();
    if (!success) {
        abort();
    }

    // Little hack - start the test framework if we're using it
    void* handle = dlopen(nullptr, RTLD_LAZY);
    assert(handle);
    auto testFunc = (load_test_system_func)dlsym(handle, "openhs_test_main");
    if (testFunc) {
        testHooks.entry = (void*)symbols.entry;
        testFunc(&testHooks);
        // Don't delete the object, since we're still using it
    } else {
        dlclose(handle);
    }
}
