//
// Created by znix on 14/11/2021.
//

#include "ftl_ship_objects.h"

bool FtlCrewMember::RepairingFire() const
{
    return this->currentRepair != nullptr && onFire > 0 && !frozen && !fighting;
}

bool FtlCrewMember::IsDeadOrDying()
{
    // Note this is a custom function, not copied from the base game (though I wouldn't be surprised if they
    // have something similar).
    return IsDead() || crewAnim->status == 3; // I assume status 3 means dying?
}

FtlCrewMember::FtlCrewMember(const FtlCrewBlueprint& bp, int shipId, bool intruder, FtlCrewAnimation* anim)
{
    // FIXME this will probably leak memory by overwriting our newly-allocated strings etc
    fn.CrewMember_Ctor(this, bp, shipId, intruder, anim);
}

FtlCrewAnimation::FtlCrewAnimation(int shipId, const FtlString& name, FtlPointf position, bool enemy)
{
    fn.CrewAnimation_Ctor(this, shipId, name, position, enemy);
}
