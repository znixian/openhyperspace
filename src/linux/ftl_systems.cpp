//
// Created by znix on 05/12/2021.
//

#include "ftl_systems.h"

float FtlOxygenSystem::GetOxygenLevel(int roomId)
{
    if (!oxygenLevels.is_index_valid(roomId))
        return 0;

    return oxygenLevels[roomId];
}

void FtlOxygenSystem::SetOxygenLevel(int roomId, float newValue)
{
    oxygenLevels[roomId] = newValue;
}
