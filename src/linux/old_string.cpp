//
// Created by znix on 13/11/2021.
//

#include "old_string.h"

// Use this to find the empty string pointer - we don't use it to allocate new
// strings but it's required for the cleanup.
#include "common/function_defs.h"

#include <malloc.h>
#include <string.h>

#include <atomic>

// Hopefully this behaves the same as the allocator FTL is linking to
static std::allocator<char> alloc;

OldString::OldString(const char* initial, int count)
{
    set(initial, count);
}

// Don't bother trying to fish out glibc's empty string instance.
OldString::OldString()
    : OldString(nullptr, 0)
{
}

OldString::OldString(const char* initial)
    : OldString(initial, strlen(initial))
{
}

OldString::OldString(const std::string& initial)
    : OldString(initial.c_str(), initial.length())
{
}

// Don't bother with copy-on-write
OldString::OldString(const OldString& initial)
    : OldString(initial.c_str(), initial.length())
{
}

OldString::~OldString()
{
    drop_storage();
}

void OldString::allocate_header(size_t capacity, bool copy_prev)
{
    int64_t blockSize = sizeof(Header) + capacity + 1; // Add one for the null terminator

    void* block = alloc.allocate(blockSize);
    memset(block, 0, blockSize);

    auto* newHdr = (Header*)block;
    char* newChars = (char*)block + sizeof(Header);
    newHdr->capacity = capacity;
    newHdr->ref_count = 0; // 0 means we're the sole owner of the data

    // Copy stuff across
    if (copy_prev) {
        const Header* oldHdr = header();
        newHdr->length = oldHdr->length;
        memcpy(newChars, chars, oldHdr->length);
    }

    // Swap over the storage we're pointing to
    drop_storage();
    chars = newChars;
}

void OldString::drop_storage()
{
    if (!chars)
        return;

    Header* hdr = header();

    // Don't touch the magic empty string value
    if (hdr == fn.linux_empty_str_ptr)
        return;

    // Atomically decrement the refcount, and if it was just one then we're supposed to get rid of it
    // This can store one of three sets of values at this point (before decrementing it):
    // -1 means the string has 'leaked' outside and we can't CoW it anymore (eg we returned an iterator to it)
    // 0 means we're the sole owner
    // 1+ means there's n-1 references to it - this is what the decrement is for.
    // Thus if the object was in state 0 or -1 we can free it, otherwise someone else is still using it.
    int oldValue = std::atomic_fetch_sub(&hdr->ref_count, 1);
    if (oldValue == -1 || oldValue == 0) {
        size_t memCap = hdr->capacity + sizeof(Header) + 1;
        alloc.deallocate((char*)hdr, memCap);
    }
    chars = nullptr;
}

void OldString::set(const char* new_data, int count)
{
    allocate_header(count, false);
    if (count != 0) {
        header()->length = count;
        memcpy(chars, new_data, count);
    }
}

OldString& OldString::operator=(const OldString& other)
{
    // Ignore self-assignment
    if (&other == this)
        return *this;

    set(other.c_str(), other.length());
    return *this;
}

OldString& OldString::operator=(const std::string& other)
{
    set(other.c_str(), other.length());
    return *this;
}

OldString& OldString::operator=(const char* other)
{
    set(other, strlen(other));
    return *this;
}
