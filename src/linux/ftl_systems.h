//
// Created by znix on 05/12/2021.
//

#pragma once

#include "common/function_defs.h"

#include "linux/ftl_ship_structure.h"

struct FtlShipSystem : public FtlRepairable {
    int systemType;
    bool needsManned;
    bool manned;
    int activeManned;
    bool boostable;
    std::pair<int, int> powerState;
    int requiredPower;
    GL_Texture* imageIcon;
    GL_Primitive* iconPrimitive;
    GL_Primitive* iconBorderPrimitive;
    GL_Primitive* iconPrimitives[5][2][2]; // WTF is this?
    FtlCachedRect partialDamageRect;
    FtlCachedRectOutline lockOutline;
    FtlRect roomShape;
    bool onFire;
    bool breached;
    std::pair<int, int> healthState;
    float damageOverTime;
    float repairOverTime;
    bool damagedLastFrame;
    bool repairedLastFrame;
    int originalPower;
    bool needsPower;
    int tempPowerCap;
    int tempPowerLoss;
    int tempDividePower;
    int lockCount;
    FtlTimerHelper lockTimer;
    bool exploded;
    bool occupied;
    bool friendlies;
    FtlString interiorImageName;
    GL_Primitive* interiorImage;
    GL_Primitive* interiorImageOn;
    GL_Primitive* interiorImageManned;
    GL_Primitive* interiorImageMannedFancy;
    int lastUserPower;
    int bonusPower;
    int lastBonusPower;
    FtlPointf location;
    int bpCost;
    FtlAnimationTracker flashTracker;
    int maxLevel;
    int batteryPower;
    int hackEffect;
    bool underAttack;
    bool levelBoostable;
    bool triggerIon;
    FtlVector<FtlAnimation> damagingEffects;
    int computerLevel;
};
static_assert(sizeof(FtlShipSystem) == 584, "ShipSystem is the wrong size");

struct FtlOxygenSystem : public FtlShipSystem {
    float maxOxygen;
    FtlVector<float> oxygenLevels;
    float totalOxygen;
    bool leakingOxygen;

    // Helper functions, copied from the game to reduce the number of functions we need to access

    /// Get the current amount of oxygen in a given room, on a scale of 0-100, or 0 for invalid rooms.
    float GetOxygenLevel(int roomId);

    /// Set the current amount of oxygen in a given room, on a scale of 0-100. This value is not clamped.
    void SetOxygenLevel(int roomId, float newValue);
};
static_assert(sizeof(FtlOxygenSystem) == 616, "OxygenSystem is the wrong size");
