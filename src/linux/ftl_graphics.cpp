//
// Created by znix on 05/12/2021.
//

#include "ftl_graphics.h"

void FtlAnimationTracker::Update()
{
    fn.AnimationTracker_Update(this);
}

void FtlAnimationTracker::SetLoop(bool enableLoop, float delay)
{
    loop = enableLoop;
    loopDelay = delay;
    currentDelay = 0.0;
}

void FtlAnimationTracker::Start(float atTime)
{
    // Decompiled and copied from FTL
    done = false;
    running = true;
    currentTime = atTime;
    reverse = false;
}
