//
// Created by znix on 07/11/2021.
//

#include "mem_map.h"

#include <cstdint>
#include <cstdio>
#include <cstring>

size_t fstream_read(void* buff, size_t elsize, size_t nelem, void* parm)
{
    return fread(buff, elsize, nelem, (FILE*)parm);
}

inline CBStream open_read_stream(const CBString& filename)
{
    FILE* fi = fopen((const char*)filename.data, "r");
    return CBStream(fstream_read, fi);
}

MemMaps::MemMaps()
{
    CBStream f = open_read_stream("/proc/self/maps");

    while (true) {
        CBString origLine = f.readLine('\n');

        // Empty origLine means break
        if (origLine.length() == 0)
            break;

        CBString line = origLine;
        line.trim();

        CBStringList items;
        items.split(line, ' ');

        // Remove any empty items caused by back-to-back spaces
        for (int i = (int)items.size() - 1; i >= 0; i--) {
            if (items.at(i).length() == 0)
                items.erase(items.begin() + i);
        }

        Region region {};
        region.file_offset = strtoll(items.at(2), nullptr, 16);
        if (items.size() >= 6)
            region.path = items.at(5);

        // Split and parses the virtual addresses

        CBStringList addrs;
        addrs.split(items.at(0), '-');

        region.vm_start = strtoll(addrs.at(0), nullptr, 16);
        region.vm_end = strtoll(addrs.at(1), nullptr, 16);

        // Ignore everything but the FTL region
        if (region.path.find("/FTL.amd64") == BSTR_ERR) {
            continue;
        }

        regions.emplace_back(std::move(region));
    }
}

const MemMaps::Region* MemMaps::translate(int64_t addr)
{
    for (const Region& r : regions) {
        if (addr >= r.file_offset && addr < r.file_end())
            return &r;
    }

    return nullptr;
}
