//
// The definitions for the stuff relating to the overall structure of the ship, such
// as it's rooms, and all the ship management stuff.
//
// 'structure' is perhaps a poor choice of words, it's not related to C structs.
//
// Created by znix on 26/11/2021.
//

#pragma once

#include "ftl_ship_objects.h"
#include "linux_functions.h"

struct FtlTargetable {
    virtual ~FtlTargetable() = default;
    // TODO virtual functions

    int type = 0;
    bool hostile = false;
    bool targeted = false;
};
static_assert(sizeof(FtlTargetable) == 16, "Targetable is the wrong size");

struct FtlCollideable {
    virtual ~FtlCollideable() = default;
    // TODO virtual functions
};
static_assert(sizeof(FtlCollideable) == 8, "Collideable is the wrong size");

struct FtlSelectable {
    virtual ~FtlSelectable() = default;

    int selectedState;

    // TODO virtual functions
};
static_assert(sizeof(FtlSelectable) == 16, "Selectable is the wrong size");

struct FtlRepairable : public FtlSelectable, public FtlShipObject {
    virtual ~FtlRepairable() = default;

    float damage;
    FtlPoint loc;
    float maxDamage;
    FtlString name;
    int roomId;
    int repairCount;

    // TODO virtual functions
};
static_assert(sizeof(FtlRepairable) == 64, "Repairable is the wrong size");

struct FtlRoom : public FtlSelectable, public FtlShipObject {
    FtlRect rect;
    int roomId;

    bool blackedOut;
    FtlVector<int> filledSlots;
    FtlVector<FtlVector<bool>> slots;
    bool warningLight;
    FtlAnimationTracker lightTracker;

    int fireCount;
    FtlVector<FtlAnimation> fires;
    int primarySlot;
    int primaryDirection;
    float lastO2;
    GL_Primitive* floorPrimitive;
    GL_Primitive* blackoutPrimitive;
    GL_Primitive* highlightPrimitive;
    GL_Primitive* highlightPrimitive2;
    GL_Primitive* o2LowPrimitive;
    GL_Primitive* computerPrimitive;
    GL_Primitive* computerGlowPrimitive;
    GL_Primitive* computerGlowYellowPrimitive;
    GL_Primitive* lightPrimitive;
    GL_Primitive* lightGlowPrimitive;
    FtlAnimation stunSparks;
    FtlAnimation consoleSparks;
    bool stunning;
    float hacked;
    int currentSparkRotation;
    FtlVector<FtlAnimation> sparks;
    float sparkTimer;
    int sparkCount;
    int hackLevel;
    FtlAnimation roomTapped;
};
static_assert(sizeof(FtlRoom) == 904, "Room is the wrong size");

// Yeah this probably shouldn't go here, TODO cleanup
struct FtlShip : public FtlShipObject {
    FtlVector<FtlRoom*> roomList;
    FtlVector<FtlDoor*> doorList;
    FtlVector<FtlOuterHull*> outerWalls;
    FtlVector<FtlDoor*> outerAirlocks;
    std::pair<int, int> hullIntegrity;
    FtlVector<FtlWeaponMount> weaponMounts;
    FtlString floorImageName;
    char _shipFloor[32]; // TODO ImageDesc
    GL_Primitive* floorPrimitive;
    FtlString shipImageName;

    char _remaining[968]; // TODO fill these out!
};
static_assert(sizeof(FtlShip) == 1168, "Ship is the wrong size");

struct FtlShipManager : public FtlShipObject, public FtlTargetable, public FtlCollideable {
    FtlVector<FtlShipSystem*> systemList;
    FtlOxygenSystem* oxygenSystem;
    FtlTeleportSystem* teleportSystem;
    FtlCloakingSystem* cloakingSystem;
    FtlBatterySystem* batterySystem;
    FtlMindSystem* mindSystem;
    FtlCloneSystem* cloneSystem;
    FtlHackingSystem* hackingSystem;
    bool showNetwork;
    bool addedSystem;
    FtlShields* shieldSystem;
    FtlWeaponSystem* weaponSystem;
    FtlDroneSystem* droneSystem;
    FtlEngineSystem* engineSystem;
    FtlMedbaySystem* medbaySystem;
    FtlVector<FtlArtillerySystem*> artillerySystems;

    FtlVector<FtlCrewMember*> crewList;
    char _fireSpreader[64]; // TODO FtlSpreader<FtlFire>
    FtlShip ship;
    char _statusMessages[80]; // TODO std::queue
    bool gameOver;
    FtlShipManager* currentTarget;
    std::pair<float, float> jumpTimer;
    int fuelCount;
    bool hostileShip;
    bool destroyed;
    int lastDamage;

    FtlAnimationTracker jumpAnimation;
    FtlVector<FtlDamageMessage*> damageMessages;
    FtlVector<int> systemKey;
    int currentScrap;
    bool jumping;
    bool automated;
    int shipLevel;

    char _myBlueprint[592]; // TODO FtlShipBlueprint
    bool lastEngineStatus;
    bool lastJumpReady;
    bool containsPlayerCrew;
    int intruderCount;
    FtlVector<FtlVector<int>> crewCounts;
    int tempDroneCount;
    int tempMissileCount;
    FtlVector<FtlAnimation> explosions;
    char _tempVision[40]; // FtlVector<bool> - TODO bool specialisation
    bool highlightCrew;

    FtlVector<FtlDrone*> droneTrash;
    FtlVector<FtlSpaceDrone*> spaceDrones;
    FtlVector<FtlSpaceDrone*> newDroneArrivals;
    int bpCount;
    int customiseMode;
    bool showRoom;

    FtlVector<FtlProjectile*> superBarrage;
    bool invincible;
    FtlVector<FtlSpaceDrone*> superDrones;
    GL_Primitive* highlight;
    int failedDodgeCounter;

    FtlVector<float> hitByBeam;
    bool enemyDamageUncloaked;
    int damageCloaked;
    char _killedByBeam[48]; // TODO std::map - this is map<int, int>
    int minBeaconHealth;
    FtlVector<FtlParticleEmitter*> fireExtinguishers;
    bool wasSafe;
};
static_assert(sizeof(FtlShipManager) == 2648);
