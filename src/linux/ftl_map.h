//
// Created by znix on 14/11/2021.
//

#pragma once

#include <memory>

// Red-black tree based map, matches std::map
template <typename K, typename V, typename Alloc = std::allocator<V>>
class FtlMap {
public:
    FtlMap()
    {
        impl.header.left = &impl.header;
        impl.header.right = &impl.header;
    }

    ~FtlMap()
    {
        if (Root())
            DeallocNode(Root());
    }

private:
    enum TreeColour {
        Red,
        Black
    };

    class NodeBase {
    public:
        TreeColour colour = Red;
        NodeBase* parent = nullptr;
        NodeBase* left = nullptr;
        NodeBase* right = nullptr;
    };
    static_assert(sizeof(NodeBase) == 32, "FtlMap::Node is the wrong size");

    class Node : public NodeBase {
    public:
        std::pair<K, V> value;

        inline Node* Right() { return (Node*)this->right; }
        inline Node* Left() { return (Node*)this->left; }
        inline Node* Parent() { return (Node*)this->parent; }
    };

    using NodeAlloc = typename std::allocator_traits<Alloc>::template rebind_alloc<Node>;

    struct Impl : NodeAlloc {
        char _lessPadding = 0; // Padding for the apparently one-byte size of std::less

        NodeBase header {};
        size_t size = 0;
    };
    Impl impl;

    Node* Root()
    {
        return (Node*)impl.header.parent;
    }

    Node* AllocNode() { return impl.allocate(1); }
    void DeallocNode(Node* node) { impl.deallocate(node, 1); }

    Node* CreateNode()
    {
        Node* node = impl.allocate(1);
        new (node) Node();
        return node;
    }
    void DestroyNode(Node* node)
    {
        node->~Node();
        DeallocNode(node);
    }

    void Erase(Node* node)
    {
        if (!node)
            return;
        Erase(node->Left());
        Erase(node->Right());
        DestroyNode(node);
    }
};
static_assert(sizeof(FtlMap<int, int>) == 48, "FtlMap is the wrong size");

template <typename K, typename V>
class FtlUnorderedMap {
private:
    struct node {
        node* next = nullptr;
        // This may, depending on policy, also contain a cached hashcode
        // This should contain the key/value pair, don't need it right now
    };

    struct storage : std::allocator<std::pair<K, V>> {
        node** buckets;
        size_t bucket_count;
        void* bbegin;
        size_t element_count;

        struct {
            float max_load_factor;
            size_t next_resize;
        } rehash_policy;
    } store;

public:
};
static_assert(sizeof(FtlUnorderedMap<int, int>) == 48, "FtlUnorderedMap is the wrong size");
