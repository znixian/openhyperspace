//
// Created by znix on 07/11/2021.
//

#include "linux_functions.h"
#include "sym_parser.h"

void linux_load_functions(FtlFunctions* functions, SymbolSet* symbols)
{
    auto& s = symbols->symbols;

    *functions = FtlFunctions {
        // CApp
        .CApp_OnExecute = (FtlCApp_OnExecute)s.at("_ZN4CApp9OnExecuteEv"),

        // CFPS
        .CFPS_GetSpeedFactor = (FtlCFPS_GetSpeedFactor)s.at("_ZN4CFPS14GetSpeedFactorEv"),
        .CFPS_FPSControl = (FtlCFPS*)s.at("_ZN4CFPS10FPSControlE"),

        // Resource Control
        .ResourceControl_RenderImage_GL = (FtlResourceControl_RenderImage_GL)s.at("_ZN15ResourceControl11RenderImageEP10GL_Textureiii8GL_Colorfb"),
        .ResourceControl_LoadFile = (FtlResourceControl_LoadFile)s.at("_ZN15ResourceControl8LoadFileERKSs"),
        .ResourceControl_GlobalResources = (FtlResourceControl*)s.at("_ZN15ResourceControl15GlobalResourcesE"),

        // CSurface
        .CSurface_DisableBlend = (FtlCSurface_DisableBlend)s.at("_ZN8CSurface15GL_DisableBlendEv"),
        .CSurface_EnableBlend = (FtlCSurface_EnableBlend)s.at("_ZN8CSurface14GL_EnableBlendEv"),
        .CSurface_DestroyPrimitive = (FtlCSurface_DestroyPrimitive)s.at("_ZN8CSurface19GL_DestroyPrimitiveEP12GL_Primitive"),

        // MainMenu
        .MainMenu_OnRender = (FtlMainMenu_OnRender)s.at("_ZN8MainMenu8OnRenderEv"),
        .MainMenu_Close = (FtlMainMenu_Close)s.at("_ZN8MainMenu5CloseEv"),

        // ShipBuilder
        .ShipBuilder_Open = (FtlShipBuilder_Open)s.at("_ZN11ShipBuilder4OpenEv"),

        // Ship
        .Ship_Import = (FtlShip_Import)s.at("_ZN4Ship6ImportESs"),

        // BlueprintManager
        .BlueprintManager_Instance = (FtlBlueprintManager*)s.at("_ZN16BlueprintManager10BlueprintsE"),
        .BlueprintManager_GetCrewBlueprint = (FtlBlueprintManager_GetCrewBlueprint)s.at("_ZN16BlueprintManager16GetCrewBlueprintESs"),
        .BlueprintManager_GetWeaponBlueprint = (FtlBlueprintManager_GetWeaponBlueprint)s.at("_ZN16BlueprintManager18GetWeaponBlueprintESs"),
        .BlueprintManager_GetShipBlueprint = (FtlBlueprintManager_GetShipBlueprint)s.at("_ZN16BlueprintManager16GetShipBlueprintERKSsi"),

        // WorldManager
        .WorldManager_CreateCommandGui = (FtlWorldManager_CreateCommandGui)s.at("_ZN12WorldManager16CreateCommandGuiEv"),
        .WorldManager_StartGame = (FtlWorldManager_StartGame)s.at("_ZN12WorldManager9StartGameEP11ShipManager"),
        .WorldManager_OnLoop = (FtlWorldManager_OnLoop)s.at("_ZN12WorldManager6OnLoopEv"),

        // ShipManager
        .ShipManager_Ctor = (FtlShipManager_Ctor)s.at("_ZN11ShipManagerC1Ei"),
        .ShipManager_OnInit = (FtlShipManager_OnInit)s.at("_ZN11ShipManager6OnInitEPK13ShipBlueprinti"),
        .ShipManager_UpdateEnvironment = (FtlShipManager_UpdateEnvironment)s.at("_ZN11ShipManager17UpdateEnvironmentEv"),
        .ShipManager_AddCrewMember_ByBlueprint = (FtlShipManager_AddCrewMember_ByBlueprint)s.at("_ZN11ShipManager13AddCrewMemberE13CrewBlueprintibib"),
        .ShipManager_AddCrewMember_ByString = (FtlShipManager_AddCrewMember_ByString)s.at("_ZN11ShipManager13AddCrewMemberESsSsbibb"),
        .ShipManager_AddCrewMember_ByObject = (FtlShipManager_AddCrewMember_ByObject)s.at("_ZN11ShipManager13AddCrewMemberEP10CrewMemberi"),

        // Equipment
        .Equipment_AddWeapon = (FtlEquipment_AddWeapon)s.at("_ZN9Equipment9AddWeaponEPK15WeaponBlueprintbb"),

        // CompleteShip
        .CompleteShip_AddCrewMember_ByBlueprint = (FtlCompleteShip_AddCrewMember_ByBlueprint)s.at("_ZN12CompleteShip13AddCrewMemberE13CrewBlueprintb"),
        .CompleteShip_AddCrewMember_ByString = (FtlCompleteShip_AddCrewMember_ByString)s.at("_ZN12CompleteShip13AddCrewMemberESsSsb"),
        .CompleteShip_AddCrewMember_ByObject = (FtlCompleteShip_AddCrewMember_ByObject)s.at("_ZN12CompleteShip13AddCrewMemberEP10CrewMemberi"),

        // ShipObject
        .ShipObject_HasEquipment = (FtlShipObject_HasEquipment)s.at("_ZN10ShipObject12HasEquipmentESs"),
        .ShipObject_GetAugmentationValue = (FtlShipObject_GetAugmentationValue)s.at("_ZN10ShipObject20GetAugmentationValueESs"),

        // CrewMember
        .CrewMember_Ctor = (FtlCrewMember_Ctor)s.at("_ZN10CrewMemberC1ERK13CrewBlueprintibP13CrewAnimation"),
        .CrewMember_DirectModifyHealth = (FtlCrewMember_DirectModifyHealth)s.at("_ZN10CrewMember18DirectModifyHealthEf"),
        .CrewMember_SaveState = (FtlCrewMember_SaveState)s.at("_ZN10CrewMember9SaveStateEi"),
        .CrewMember_LoadState = (FtlCrewMember_LoadState)s.at("_ZN10CrewMember9LoadStateEi"),
        .CrewMember_OnLoop = (FtlCrewMember_OnLoop)s.at("_ZN10CrewMember6OnLoopEv"),
        .CrewMember_OnRender = (FtlCrewMember_OnRender)s.at("_ZN10CrewMember8OnRenderEb"),
        .CrewMember_SetOutOfGame = (FtlCrewMember_SetOutOfGame)s.at("_ZN10CrewMember12SetOutOfGameEv"),
        .CrewMember_ShipDamage = (FtlCrewMember_ShipDamage)s.at("_ZN10CrewMember10ShipDamageEf"),
        .CrewMember_UpdateRepair = (FtlCrewMember_UpdateRepair)s.at("_ZN10CrewMember12UpdateRepairEv"),
        .CrewMember_UpdateHealth = (FtlCrewMember_UpdateHealth)s.at("_ZN10CrewMember12UpdateHealthEv"),

        .CrewMemberFactory_Instance = (FtlCrewMemberFactory*)s.at("_ZN17CrewMemberFactory7FactoryE"),
        .CrewMemberFactory_CreateCrewmember = (FtlCrewMemberFactory_CreateCrewmember)s.at("_ZN17CrewMemberFactory16CreateCrewmemberEPK13CrewBlueprintib"),

        .CrewAnimation_Ctor = (FtlCrewAnimation_Ctor)s.at("_ZN13CrewAnimationC1EiSs6Pointfb"),

        .AnimationTracker_Update = (FtlAnimationTracker_Update)s.at("_ZN16AnimationTracker6UpdateEv"),

        .GenericButton_MouseMove = (FtlGenericButton_MouseMove)s.at("_ZN13GenericButton9MouseMoveEiib"),
        .TextButton_OnRender = (FtlTextButton_OnRender)s.at("_ZN10TextButton8OnRenderEv"),
        .TextButton_ResetPrimitives = (FtlTextButton_ResetPrimitives)s.at("_ZN10TextButton15ResetPrimitivesEv"),
        .TextButton_GetIdealButtonWidth = (FtlTextButton_GetIdealButtonWidth)s.at("_ZN10TextButton19GetIdealButtonWidthEv"),

        .Room_Ctor = (FtlRoom_ctor)s.at("_ZN4RoomC1Eiiiiii"),

        // /////////
        // Blueprints etc
        // /////////

        // /////////
        // Systems
        // /////////
        .OxygenSystem_ComputeAirLoss = (FtlOxygenSystem_ComputeAirLoss)s.at("_ZN12OxygenSystem14ComputeAirLossEifb"),

        // /////////
        // Platform-specific
        // /////////
        .linux_empty_str_ptr = s.at("_ZNSs4_Rep20_S_empty_rep_storageE"),
    };
}
