//
// Created by znix on 07/11/2021.
//

#pragma once

#include <bstrwrap.h>

class MemMaps {
public:
    struct Region {
        int64_t vm_start, vm_end; // Virtual memory addresses
        int64_t file_offset;
        CBString path;
        [[nodiscard]] inline int64_t length() const { return vm_end - vm_start; }
        [[nodiscard]] inline int64_t file_end() const { return file_offset + length(); }

        // Get an address that can be added to a file address to get a pointer to the data in this section in VM space
        [[nodiscard]] inline int64_t virt_addr() const { return vm_start - file_offset; }
    };

    explicit MemMaps();

    /**
     * Convert a file-address to a VM-address.
     */
    const MemMaps::Region* translate(int64_t addr);

    std::vector<Region> regions;
};
