//
// Created by znix on 05/12/2021.
//

#include "ftl_gui.h"

void FtlGenericButton::Reset()
{
    hover = false;
    activated = false;
    selected = false;
    activeTouch = 0;
}

void FtlGenericButton::SetLocation(FtlPoint newPosition)
{
    if (newPosition == position) {
        return;
    }

    hitbox.x += newPosition.x - position.x;
    hitbox.y += newPosition.y - position.y;
    position = newPosition;
    ResetPrimitives();
}

void FtlGenericButton::SetHitBox(const FtlRect& newHitbox)
{
    hitbox = newHitbox;
}

void FtlGenericButton::SetActive(bool newActive)
{
    active = newActive;
    if (!active) {
        activated = false;
        selected = false;
    }
}

void FtlGenericButton::OnLoop()
{
    flashing.Update();
}

void FtlGenericButton::OnRender()
{
}

void FtlGenericButton::MouseMove(int mX, int mY, bool silent)
{
    fn.GenericButton_MouseMove(this, mX, mY, silent);
}

void FtlGenericButton::OnClick()
{
}

void FtlGenericButton::OnRightClick()
{
}

void FtlGenericButton::OnTouch()
{
    abort(); // Don't bother implementing
}

void FtlGenericButton::ResetPrimitives()
{
}

void FtlTextButton::OnRender()
{
    fn.TextButton_OnRender(this);
}

void FtlTextButton::ResetPrimitives()
{
    fn.TextButton_ResetPrimitives(this);
}

int FtlTextButton::GetIdealButtonWidth()
{
    return fn.TextButton_GetIdealButtonWidth(this);
}
