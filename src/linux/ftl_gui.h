//
// Created by znix on 05/12/2021.
//

#pragma once

#include "ftl_graphics.h"
#include "ftl_structures.h"

enum FtlAllowedCharType : int {
    ALLOW_ASCII,
    ALLOW_LANGUAGE,
    ALLOW_ANY,
};

struct FtlFocusWindow {
    virtual ~FtlFocusWindow() = default;

    bool open;
    bool fullFocus;
    FtlPoint close;
    bool closeButtonSelected;
    FtlPoint position;
};
static_assert(sizeof(FtlFocusWindow) == 32);

struct FtlResourceEvent {
    int missiles;
    int fuel;
    int drones;
    int scrap;
    int crew;
    bool traitor;
    bool cloneable;
    FtlTextString cloneText;
    FtlString crewType;
    FtlWeaponBlueprint* weapon;
    FtlDroneBlueprint* drone;
    FtlAugmentBlueprint* augment;
    FtlCrewBlueprint crewBlue;
    int systemId;
    int weaponCount;
    int droneCount;
    bool steal;
    bool intruders;
    int fleetDelay;
    int hullDamage;
    int upgradeAmount;
    int upgradeId;
    int upgradeSuccessFlag;
    FtlString removeItem;
};
static_assert(sizeof(FtlResourceEvent) == 376, "ResourceEvent is the wrong size");

struct FtlGenericButton {
    virtual ~FtlGenericButton() = default;
    virtual void Reset();
    virtual void SetLocation(FtlPoint newPosition);
    virtual void SetHitBox(const FtlRect& newHitbox);
    virtual void SetActive(bool newActive);
    virtual void OnLoop();
    virtual void OnRender();
    virtual void MouseMove(int mX, int mY, bool silent);
    virtual void OnClick();
    virtual void OnRightClick();
    virtual void OnTouch(); // Note signature is wrong!
    virtual void ResetPrimitives();

    FtlPoint position;
    FtlRect hitbox = { -1, -1, 0, 0 };
    bool allowAnyTouch = true;
    bool touchSelectable = false;
    bool renderOff = false;
    bool renderSelected = false;
    bool bFlashing = false;
    FtlAnimationTracker flashing;
    bool active = true;
    bool hover = false;
    bool activated = false;
    bool selected = false;
    int activeTouch = 0;

    FtlGenericButton()
    {
        flashing.SetLoop(true, 0.0);
        flashing.Start(0.0);
    }
};
static_assert(sizeof(FtlGenericButton) == 80, "GenericButton is the wrong size");

struct FtlChoiceText {
    int type;
    FtlString text;
    FtlResourceEvent rewards;
};
static_assert(sizeof(FtlChoiceText) == 392, "ChoiceText is the wrong size");

struct FtlChoiceBox : public FtlFocusWindow {
    GL_Texture* textBox;
    FtlWindowFrame* box;
    FtlString mainText;
    FtlVector<FtlChoiceText> choices;
    unsigned int columnSize;
    FtlVector<FtlRect> choiceBoxes;
    int potentialChoice;
    int selectedChoice;
    int fontSize;
    bool centred;
    int gapSize;
    float openTime;
    FtlResourceEvent rewards;
    GL_Colour currentTextColour;
    FtlPointf lastChoice;
};
static_assert(sizeof(FtlChoiceBox) == 536, "ChoiceBox is the wrong size");

struct FtlSlideBar {
    FtlRect box;
    bool hovering;
    bool holding;
    FtlRect marker;
    FtlPoint mouseStart;
    FtlPoint rectStart;
    std::pair<int, int> minMax;
};
static_assert(sizeof(FtlSlideBar) == 60, "SlideBar is the wrong size");

struct FtlButton : public FtlGenericButton {
    GL_Texture* images[3] = { nullptr, nullptr, nullptr };
    GL_Primitive* primitives[3] = { nullptr, nullptr, nullptr };
    FtlPoint imageSize;
    bool mirror = false;
};
static_assert(sizeof(FtlButton) == 144, "Button is the wrong size");

struct FtlControlButton {
    FtlRect rect;
    FtlString value;
    FtlTextString desc;
    FtlString key;
    int state;
    int descLength;
};
static_assert(sizeof(FtlControlButton) == 56, "ControlButton is the wrong size");

struct FtlTextButton : public FtlGenericButton {
    void OnRender() override;
    void ResetPrimitives() override;
    virtual int GetIdealButtonWidth();

    GL_Primitive* primitives[3];
    GL_Texture* baseImage;
    FtlPoint baseImageOffset;
    GL_Primitive* basePrimitive;
    bool coloursSet;
    GL_Colour colours[3];
    GL_Colour textColour;
    FtlPoint buttonSize;
    int cornerInset;
    bool autoWidth;
    int autoWidthMargin;
    int autoWidthMin;
    bool autoRightAlign;
    FtlTextString label;
    int font;
    int lineHeight;
    int textYOffset;
    bool autoShrink;
};
static_assert(sizeof(FtlTextButton) == 256, "TextButton is the wrong size");

struct FtlTextInput {
    FtlString prompt;
    FtlVector<int> text;
    FtlVector<int> oldText;
    int pos;
    int lastPos;
    bool active;
    FtlAllowedCharType allowedChars;
    int maxChars;
    FtlTimerHelper blinker;
};
static_assert(sizeof(FtlTextInput) == 96, "TextInput is the wrong size");

struct FtlDropBox {
    FtlPoint position;
    bool isSellBox;
    GL_Texture* boxImage[2];
    int selectedImage;
    FtlTextString titleText;
    FtlTextString bodyText;
    int bodySpace;
    FtlTextString lowerText;
    FtlTextString sellText;
    FtlString sellCostText;
    int textWidth;
    int insertHeight;
    int titleInsert;
};
static_assert(sizeof(FtlDropBox) == 136, "DropBox is the wrong size");

struct FtlEquipmentBoxItem {
    FtlProjectileFactory* weapon;
    FtlDrone* drone;
    FtlCrewMember* crew;
    FtlAugmentBlueprint* augment;
};
static_assert(sizeof(FtlEquipmentBoxItem) == 32, "EquipmentBoxItem is the wrong size");

struct FtlEquipmentBox {
    virtual ~FtlEquipmentBox() = default;

    GL_Primitive* blockedOverlay;
    GL_Colour overlayColour;
    FtlString imageName;
    GL_Primitive* empty;
    GL_Primitive* full;
    GL_Primitive* selectedEmpty;
    GL_Primitive* selectedFull;
    FtlWeaponSystem* weaponSys;
    FtlDroneSystem* droneSys;
    FtlPoint location;
    FtlRect hitBox;
    FtlEquipmentBoxItem item;
    bool mouseHovering;
    bool glow;
    bool blocked;
    int slot;
    bool locked;
    int value;
    bool permanentLock;
    bool blockDetailed;
};
static_assert(sizeof(FtlEquipmentBox) == 168, "EquipmentBox is the wrong size");

struct FtlCrewEquipBox : FtlEquipmentBox {
    FtlShipManager* ship;
    bool dead;
    FtlTextButton deleteButton;
    FtlTextButton renameButton;
    bool showDelete;
    bool showRename;
    bool quickRenaming;
    FtlTextInput nameInput;
    GL_Primitive* box;
    GL_Primitive* boxOn;
    bool confirmDelete;
};
static_assert(sizeof(FtlCrewEquipBox) == 824, "CrewEquipBox is the wrong size");

struct FtlStarMap;

struct FtlWarningMessage {
    virtual ~FtlWarningMessage() = default;

    FtlAnimationTracker tracker;
    FtlPoint position;
    bool isImage;
    FtlTextString text;
    bool centreText;
    GL_Colour textColour;
    bool useWarningLine;
    FtlCachedImage image;
    FtlString imageName;
    bool flash;
    FtlString sound;
    FtlAnimationTracker flashTracker;
};
static_assert(sizeof(FtlWarningMessage) == 224, "WarningMessage is the wrong size");

struct FtlConfirmWindow : public FtlFocusWindow {
    FtlTextString text;
    int textHeight;
    int minWidth;
    int windowWidth;
    FtlTextString yesText;
    FtlTextString noText;
    bool autoCentre;
    GL_Texture* windowImage;
    GL_Primitive* window;
    FtlTextButton yesButton;
    FtlTextButton noButton;
    bool result;
};
static_assert(sizeof(FtlConfirmWindow) == 640, "ConfirmWindow is the wrong size");

struct FtlInfoBox {
    FtlPoint location;
    FtlSystemBlueprint* blueprint;
    FtlDescription desc;
    int tempUpgrade;
    int powerLevel;
    int maxPower;
    int systemId;
    int systemWidth;
    int yShift;
    FtlPoint descBoxSize;
    FtlCrewBlueprint* crewBlueprint;
    FtlString warning;
    bool detailed;
    FtlString additionalTip;
    FtlString additionalWarning;
    FtlWindowFrame* primaryBox;
    int primaryBoxOffset;
    FtlWindowFrame* secondaryBox;
    FtlString droneBlueprint;
};
static_assert(sizeof(FtlInfoBox) == 216, "InfoBox is the wrong size");

struct FtlStoreBox {
    virtual ~FtlStoreBox() = default;

    int itemId;
    int itemBox;
    FtlString buttonImage;
    FtlButton button;
    GL_Primitive* symbol;
    FtlDescription desc;
    int count;
    int costPosition;
    FtlShipManager* shopper;
    FtlEquipment* equipScreen;
    FtlBlueprint* blueprint;
    bool equipmentBox;
    float iconScale;
    FtlPoint pushIcon;
};
static_assert(sizeof(FtlStoreBox) == 320, "StoreBox is the wrong size");

struct FtlStore : public FtlFocusWindow {
    GL_Texture* box;
    FtlTextString headingTitle[4];
    FtlButton page1;
    FtlButton page2;
    FtlConfirmWindow confirmDialogue;
    FtlButton* currentButton;
    FtlDescription currentDescription;
    FtlString unavailable;
    FtlVector<FtlStoreBox*> storeBoxes;
    FtlVector<FtlStoreBox*> itemBoxes;
    FtlShipManager* shopper;
    int selectedWeapon;
    int selectedDrone;
    FtlInfoBox infoBox;
    FtlPoint infoBoxLoc;
    FtlButton exitButton;
    int worldLevel;
    int sectionCount;
    int types[4];
    bool showPage2;
    FtlStoreBox* confirmBuy;
    int forceSystemInfoWidth;
};
static_assert(sizeof(FtlStore) == 1624, "Store is the wrong size");

struct FtlMenuScreen : public FtlFocusWindow {
    GL_Texture* mainImage;
    GL_Primitive* menuPrimitive;
    int menuWidth;
    FtlVector<FtlTextButton*> buttons;
    int command;
    FtlVector<int> commands;
    FtlPoint position;
    FtlConfirmWindow confirmDialogue;
    int tempCommand;
    FtlGenericButton* saveQuit;
    bool showControls;
    FtlPoint statusPosition;
    GL_Texture* difficultyBox;
    int difficultyWidth;
    FtlString difficultyLabel;
    FtlString difficultyText;
    GL_Texture* dlcBox;
    int dlcWidth;
    FtlString dlcLabel;
    FtlString dlcText;
    GL_Texture* achBox;
    GL_Primitive* achBoxPrimitive;
    int achWidth;
    FtlString achLabel;
    FtlVector<FtlShipAchivementInfo> shipAchievements;
    int selectedAch;
    FtlInfoBox info;
};
static_assert(sizeof(FtlMenuScreen) == 1136, "MenuScreen is the wrong size");

struct FtlCreditScreen {
    float scroll;
    float scrollSpeed;
    FtlString shipName;
    FtlString crewString;
    float pausing;
    GL_Texture* bg;
    FtlVector<FtlString> creditNames;
    int lastValidCredit;
    int touchesDown;
    double touchDownTime;
    float skipMessageTime;
};
static_assert(sizeof(FtlCreditScreen) == 88, "CreditScreen is the wrong size");

struct FtlGameOver : public FtlFocusWindow {
    FtlVector<FtlTextButton*> buttons;
    GL_Primitive* box;
    int boxWidth;
    int command;
    FtlVector<int> commands;
    bool showStats;
    FtlPoint position;
    FtlString gameoverText;
    bool victory;
    float openedTimer;
    FtlCreditScreen credits;
    bool showingCredits;
};
static_assert(sizeof(FtlGameOver) == 224, "GameOver is the wrong size");

struct FtlControlsScreen {
    FtlVector<FtlControlButton> buttons[4];
    int selectedButton;
    FtlTextButton defaultButton;
    FtlConfirmWindow resetDialogue;
    FtlButton pageButtons[4];
    int currentPage;
    FtlWindowFrame* customBox;
};
static_assert(sizeof(FtlControlsScreen) == 1592, "ControlsScreen is the wrong size");

struct FtlLanguageChooser : public FtlFocusWindow {
    FtlVector<FtlTextButton*> buttons;
    int choice;
};
static_assert(sizeof(FtlLanguageChooser) == 64, "LanguageChooser is the wrong size");

struct FtlOptionsScreen : public FtlChoiceBox {
    FtlPoint position;
    FtlPoint wipeProfilePosition;
    FtlSlideBar soundVolume;
    FtlSlideBar musicVolume;
    bool customiseControls;
    FtlControlsScreen controls;
    FtlTextButton closeButton;
    FtlTextButton wipeProfileButton;
    int choiceFullscreen;
    int choiceVSync;
    int choiceFrameLimit;
    int choiceLowend;
    int choiceColourblind;
    int choiceLanguage;
    int choiceDialogueKeys;
    int choiceShowPaths;
    int choiceAchievementPopups;
    int choiceAutoPause;
    int choiceTouchAutoPause;
    int choiceControls;
    int lastFullScreen;
    bool isSoundTouch;
    bool isMusicTouch;
    FtlLanguageChooser languageChooser;
    bool showWipeButton;
    FtlConfirmWindow wipeProfileDialogue;
    FtlChoiceBox restartRequiredDialogue;
};
static_assert(sizeof(FtlOptionsScreen) == 4088, "OptionsScreen is the wrong size");

struct FtlInputBox : public FtlFocusWindow {
    FtlWindowFrame* textBox;
    FtlString mainText;
    bool done;
    bool invertCaps;
    FtlString inputText;
    FtlVector<FtlString> lastInputs;
    int lastInputsIndex;
};
static_assert(sizeof(FtlInputBox) == 96, "InputBox is the wrong size");

struct FtlTabbedWindow : public FtlFocusWindow {
    FtlVector<FtlButton*> buttons;
    FtlVector<FtlFocusWindow*> windows;
    FtlVector<FtlString> names;
    unsigned int currentTab;
    int buttonType;
    FtlTextButton doneButton;
    FtlPoint move;
    bool blockClose;
    bool tutorialMode;
    bool windowLock;
};
static_assert(sizeof(FtlTabbedWindow) == 384, "TabbedWindow is the wrong size");

struct FtlUpgradeBox {
    FtlShipSystem* system;
    FtlShipManager* ship;
    FtlSystemBlueprint* blueprint;
    FtlPoint location;
    int tempUpgrade;
    FtlButton* currentButton;
    FtlString buttonBaseName;
    FtlButton maxButton;
    FtlButton boxButton;
    bool subsystem;
    bool isDummy;
    GL_Primitive* dummyBox;
};
static_assert(sizeof(FtlUpgradeBox) == 360, "UpgradeBox is the worng size");

struct FtlReactorButton : FtlButton {
    int tempUpgrade;
    FtlShipManager* ship;
    bool selected;
};
static_assert(sizeof(FtlReactorButton) == 160, "ReactorButton is the wrong size");

struct FtlUpgrades : public FtlFocusWindow {
    GL_Texture* box;
    FtlVector<FtlUpgradeBox*> upgradeBoxes;
    FtlShipManager* shipManager;
    FtlTextButton undoButton;
    FtlReactorButton reactorButton;
    FtlInfoBox infoBox;
    FtlPoint infoBoxLoc;
    int systemCount;
    int forceSystemInfoWidth;
};
static_assert(sizeof(FtlUpgrades) == 720, "Upgrades is the wrong size");

struct FtlCrewManifest : FtlFocusWindow {
    GL_Primitive* box;
    FtlDropBox overBox;
    FtlShipManager* shipManager;
    FtlVector<FtlCrewEquipBox*> crewBoxes;
    FtlInfoBox infoBox;
    int confirmingDelete;
    FtlConfirmWindow deleteDialogue;
};
static_assert(sizeof(FtlCrewManifest) == 1072, "CrewManifest is the wrong size");

struct FtlCommandGui {
    char _shipStatus[616]; // TODO FtlShipStatus
    char _crewControl[552]; // TODO FtlCrewControl
    char _sysControl[248]; // TODO FtlSystemControl
    char _combatControl[4512]; // TODO FtlCombatControl
    char _ftlButton[416]; // TODO FtlFTLButton
    char _spaceStatus[160]; // TODO FtlSpaceStatus
    FtlStarMap* starMap;
    FtlCompleteShip* shipComplete;
    FtlVector<FtlFocusWindow*> focusWindows;
    FtlPoint pauseTextLoc;
    GL_Primitive* pauseImage;
    GL_Primitive* pauseImage2;
    GL_Primitive* pauseImageAuto;
    GL_Primitive* pauseCrewImage;
    GL_Primitive* pauseDoorsImage;
    GL_Primitive* pauseHackingImage;
    GL_Primitive* pauseMindImage;
    GL_Primitive* pauseRoomImage;
    GL_Primitive* pauseTargetImage;
    GL_Primitive* pauseTargetBeamImage;
    GL_Primitive* pauseTeleportLeaveImage;
    GL_Primitive* pauseTeleportArriveImage;
    GL_Primitive* flareImage;
    FtlPoint shipPosition;
    FtlString locationText;
    FtlString loadEvent;
    int loadSector;
    FtlChoiceBox choiceBox;
    bool gameover;
    bool alreadyWon;
    bool outOfFuel;
    FtlMenuScreen menuBox;
    FtlGameOver gameOverScreen;
    FtlOptionsScreen optionsBox;
    bool paused;
    bool autoPaused;
    bool menuPause;
    bool eventPause;
    bool touchPause;
    int touchPauseReason;
    FtlInputBox inputBox;
    float shakeTimer;
    FtlTabbedWindow shipScreens;
    FtlTabbedWindow storeScreens;
    FtlUpgrades upgradeScreen;
    FtlCrewManifest crewScreen;
    FtlEquipment equipment;
    FtlLocation* newLocation;

    // char _todo16192[2136];
    FtlSpaceManager* space;
    FtlButton upgradeButton;
    FtlWarningMessage upgradeWarning;
    FtlTextButton storeButton;
    FtlButton optionsButton;
    float pauseAnimTime;
    float pauseAnimation;
    FtlVector<FtlStore*> storeTrash;
    FtlTimerHelper flickerTimer;
    FtlTimerHelper showTimer;
    bool hideUI;
    FtlCompleteShip* enemyShip;
    bool waitLocation;
    bool lastLocationWait;
    bool dangerLocation;
    FtlVector<int> commandKey;
    bool jumpComplete;
    int mapId;
    FtlConfirmWindow leaveCrewDialogue;
    bool secretSector;
    int activeTouch;
    bool activeTouchIsButton;
    bool activeTouchIsCrewBox;
    bool activeTouchIsShip;
    bool activeTouchIsNull;
    FtlVector<int> extraTouches;
    bool tutorialWasRunning;
    bool focusAteMouse;
    bool choiceBoxOpen;
    int systemDetailsWidth;
    FtlChoiceBox writeErrorDialogue;
    bool suppressWriteError;
};
static_assert(sizeof(FtlCommandGui) == 18328, "CommandGui is the wrong size");
