//
// Created by znix on 13/11/2021.
//

#pragma once

#include <stdint.h>

#include <atomic>
#include <string>

// Wrapper for handling old (pre-GCC-5) libstdc++ strings, which used a non-standards-conformant copy-on-write
// system.

struct OldString {
private:
    char* chars = nullptr;

    struct Header {
        size_t length;
        size_t capacity;
        std::atomic<int> ref_count; // Hopefully this is interoperable with C
    };
    static_assert(sizeof(Header) == 24, "Old string header is the wrong size");

    [[nodiscard]] inline const Header* header() const { return ((Header*)chars) - 1; }
    [[nodiscard]] inline Header* header() { return ((Header*)chars) - 1; }

    void drop_storage();
    void allocate_header(size_t capacity, bool copy_prev);

public:
    OldString();
    OldString(const char* initial, int count);
    OldString(const char* initial); // NOLINT(google-explicit-constructor)
    OldString(const std::string& initial); // NOLINT(google-explicit-constructor)
    OldString(const OldString& initial);
    ~OldString();

    void set(const char* new_data, int count);

    OldString& operator=(OldString const& other);
    OldString& operator=(const std::string& other);
    OldString& operator=(const char* other);

    [[nodiscard]] inline const char* data() const { return chars; }
    [[nodiscard]] inline const char* c_str() const { return chars; }
    [[nodiscard]] inline std::string cxx_str() const { return std::string(chars, length()); }

    [[nodiscard]] inline int64_t length() const { return header()->length; }
};
