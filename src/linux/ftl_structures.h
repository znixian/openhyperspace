//
// Created by znix on 13/11/2021.
//

#pragma once

#include "common/function_defs.h"

#include "ftl_map.h"
#include "old_string.h"

struct CEvent {
    void* cevent_maybe_vtable;
};
static_assert(sizeof(CEvent) == 8, "CEvent is the wrong size");

typedef OldString FtlString;
static_assert(sizeof(FtlString) == 8, "FtlString is the wrong size");

struct GL_Colour {
    float r, g, b, a;
};
static_assert(sizeof(GL_Colour) == 16, "GL_Colour is the wrong size");

struct FtlPoint {
    int x, y;
    FtlPoint()
        : x(-0x7fffffff)
        , y(-0x7fffffff)
    {
    }
    FtlPoint(int x, int y)
        : x(x)
        , y(y)
    {
    }

    bool operator==(const FtlPoint& other) const
    {
        return x == other.x && y == other.y;
    }
};
static_assert(sizeof(FtlPoint) == 8, "Point is the wrong size");

struct FtlPointf {
    float x, y;
    FtlPointf()
        : x(0)
        , y(0)
    {
    }
    FtlPointf(float x, float y)
        : x(x)
        , y(y)
    {
    }
};
static_assert(sizeof(FtlPointf) == 8, "Pointf is the wrong size");

struct FtlRect {
    int x, y;
    int w, h;
};
static_assert(sizeof(FtlRect) == 16, "Rect is the wrong size");

struct FtlCApp : public CEvent {
    bool running;
    bool shift_held;
    char _pad10[6];
    FtlCommandGui* gui;
    FtlWorldManager* world;
    char _main_menu[14280]; // TODO
    char _language_chooser[64]; // TODO
    int screen_x, screen_y;
    int modifier_x, modifier_y;
    bool fullScreenLastState;
    bool minimised, minLastState;
    bool focus, focusLastState;
    bool steamOverlay, steamOverlayLastState;
    bool rendering;
    bool gameLogic;
    char _pad14401[3];
    float mouseModifier_x, mouseModifier_y;
    char _pad14412[4];
    GL_FrameBuffer* framebuffer;
    bool fboSupport;
    char _pad14425[3];
    int x_bar, y_bar;
    bool lCtrl;
    bool useFrameBuffer;
    bool manualResolutionError;
    char _pad14439[1];
    int manualResErrorX, manualResErrorY;
    bool nativeFullScreenError;
    bool fbStretchError;
    char _pad14450[6];
    FtlString lastLanguage;
    bool inputFocus;
    char _pad14465[7];
};
static_assert(sizeof(FtlCApp) == 14472, "CApp is the wrong size");

struct FtlEquipment {
    char _super[32]; // TODO FocusWindow
    GL_Texture* box;
    GL_Texture* storeBox;
    char _overBox[136]; // TODO DropBox
    char _overAugImage[136]; // TODO DropBox
    char _sellBox[136]; // TODO DropBox
    bool sellingItem;
    FtlShipManager* shipManager;
    FtlVector<FtlEquipmentBox*> equipmentBoxes;
    FtlVector<FtlProjectileFactory*> weaponsTrashList;
    FtlEquipmentBox* overcapacityBox;
    FtlAugmentEquipBox* overAugBox;
    int selectedEquipBox;
    int draggingEquipBox;
    int potentialDraggingBox;
    bool dragging;
    FtlPoint firstMouse, currentMouse;
    FtlPoint dragBoxCentre, dragBoxOffset;
    char _infoBox[216]; // TODO InfoBox
    FtlString sellCostText;
    bool overCapacity;
    bool overAugCapacity;
    bool storeMode;
    int cargoId;
    FtlPoint infoBoxLoc;
};
static_assert(sizeof(FtlEquipment) == 824, "Equipment is the wrong size");

struct FtlWorldManager {
    FtlCompleteShip* playerShip;
    FtlBossShip* bossShip;
    char _spaceManager[1208]; // TODO
    int currentDifficulty;
    char _pad1228[4];
    FtlVector<FtlCompleteShip*> ships;
    char _starMap[3280]; // TODO
    FtlCommandGui* commandGui;
    FtlLocationEvent* baseLocationEvent;
    FtlLocationEvent* lastLocationEvent;
    char _shipEvent[816]; // TODO
    FtlVector<char /* TODO FtlStatusEffect */> currentEffects;
    FtlString startingText;
    FtlLocation* location;
    bool bStartedGame, bLoadingGame, vAutoSaved, bExtraChoice;
    char _pad5420[4];
    FtlVector<int> choiceHistory;
    FtlString generatedEvent;
    char _lastMainText[16]; // TODO FtlTextString
    int playerCrewCount;
    int killedCrew;
    int playerHull;
    char _pad5484[4];
    FtlVector<int> blueRaceChoices;
    int lastSelectedCrewSeed;
    bool testingBlueprints;
    char _pad5517[3];
    FtlVector<char /* TODO LocationEvent::Choice */> originalChoiceList;
};
static_assert(sizeof(FtlWorldManager) == 5544, "WorldManager is the wrong size");

struct FtlCrewAI {
    FtlShipManager* ship;
    bool aIon;
    bool airlockRequested;
    bool medbayRequested;
    bool hurtCrew;
    bool calmShip;
    FtlVector<FtlCrewMember*> crewList;
    FtlVector<FtlCrewMember*> intruderList;
    FtlVector<FtlRepairable*> hullBreaches;
    FtlVector<int /* FtlCrewTask */> desiredTaskList;
    FtlVector<int /* FtlCrewTask */> bonusTaskList;
    char _breachedRooms[40]; // TODO FtlVector<bool> special handling
    int teleportRequest;
    bool urgentTeleport;
    int startingCrewCount;
    bool multiracialCrew;
    bool overrideRace;
};
static_assert(sizeof(FtlCrewAI) == 192, "CrewAI is the wrong size");

struct FtlCombatAI {
    FtlShipManager* target;
    FtlVector<FtlProjectileFactory*> weapons;
    FtlVector<FtlSpaceDrone*> drones;
    int stances;
    FtlVector<int> systemTargets;
    bool firingWhileCloaked;
    FtlShipManager* self;
};
static_assert(sizeof(FtlCombatAI) == 104, "CombatAI is the wrong size");

struct FtlShipAI {
    FtlShipManager* ship;
    FtlShipManager* target;
    FtlCrewAI crewAI;
    FtlCombatAI combatAI;
    bool playerShip;
    bool surrendered;
    bool escaping;
    bool destroyed;
    int surrenderThreshold;
    int escapeThreshold;
    float escapeTimer;
    int lastMaxPower;
    char _pad332[4];
    char _powerProfiles[48]; // struct map<FtlString, FtlPowerProfile> powerProfiles;
    int boardingProfile;
    int iTeleportRequest;
    int iTeleportTarget;
    int brokenSystems;
    int boardingAi;
    int crewNeeded;
    bool stalemateTrigger;
    float stalemateTimer;
    int lastHealth;
    bool boss;
    int timesTeleported;
};
static_assert(sizeof(FtlShipAI) == 432, "ShipAI is the wrong size");

struct FtlCompleteShip {
    virtual ~FtlCompleteShip() = default; // This forces an implicit vtable

    int iShipId;
    FtlShipManager* shipManager;
    void* spaceManager; // TODO FtlSpaceManager*
    FtlCompleteShip* enemyShip;
    bool bPlayerShip;
    FtlShipAI shipAI;
    FtlVector<FtlCrewMember*> arrivingParty;
    FtlVector<FtlCrewMember*> leavingParty;
    int teleTargetRoom;
};
static_assert(sizeof(FtlCompleteShip) == 536, "CompleteShip is the wrong size");

struct FtlTextString {
    FtlString str;
    bool isLiteral;
};
static_assert(sizeof(FtlTextString) == 16, "TextString is the wrong size");

struct FtlDescription {
    FtlTextString title;
    FtlTextString shortTitle;
    FtlTextString description;
    int cost;
    int rarity;
    int baseRarity;
    int bp;
    bool locked;
    char _pad65[7];
    FtlTextString tooltip;
    FtlString tip;
};
static_assert(sizeof(FtlDescription) == 96, "Description is the wrong size");

struct FtlBlueprint {
    // Note: no destructor, it doesn't do anything other than destroy all the fields - which is
    // generated automatically for us too.
    void* vtable;
    FtlString name;
    FtlDescription description;
    int type;
    char _pad116[4];
};
static_assert(sizeof(FtlBlueprint) == 120, "Blueprint is the wrong size");

struct FtlCrewBlueprint : public FtlBlueprint {
    FtlTextString crewName;
    FtlTextString crewNameLong;
    FtlVector<FtlTextString> powers;
    bool male = false;
    char _pad177[7] {};
    // Have to set a constructor for this, they do in the base game too
    // These are all 2x the latter column found here: https://ftl.fandom.com/wiki/Skills
    FtlVector<std::pair<int, int>> skillLevel {
        std::make_pair(0, 30), // Piloting
        std::make_pair(0, 30), // Engines
        std::make_pair(0, 110), // Shields
        std::make_pair(0, 130), // Weapons
        std::make_pair(0, 36), // Repair
        std::make_pair(0, 16), // Combat
    };
    FtlVector<FtlVector<GL_Colour>> colourLayers;
    FtlVector<int> colourChoices;
};
static_assert(sizeof(FtlCrewBlueprint) == 256, "CrewBlueprint is the wrong size");

struct FtlShipBlueprint : public FtlBlueprint {
    struct SystemTemplate {
        int systemId;
        int powerLevel;
        FtlVector<int> location;
        int bp;
        int maxPower;
        FtlString image;
        int slot;
        int direction;
        FtlVector<FtlString> weapon;
    };

    FtlDescription desc;
    FtlString blueprintName;
    FtlTextString name;
    FtlTextString shipClass;
    FtlString layoutFile;
    FtlString imgFile;
    FtlString cloakFile;
    FtlString shieldFile;
    FtlString floorFile;
    FtlMap<int, SystemTemplate> systemInfo;
    FtlVector<int> systems;
    int droneCount;
    int originalDroneCount;
    int droneSlots;
    FtlString loadDrones;
    FtlVector<FtlString> drones;
    FtlVector<FtlString> augments;
    int weaponCount;
    int originalWeaponCount;
    int weaponSlots;
    FtlString loadWeapons;
    FtlVector<FtlString> weapons;
    int missiles;
    int drone_count; // FIXME this name is obviously confusing with droneCount
    int health;
    int originalCrewCount;
    FtlVector<FtlString> defaultCrew;
    FtlVector<FtlCrewBlueprint> customCrew;
    int maxPower;
    int boardingAi;
    int bpCount;
    int maxCrew;
    int maxSector;
    int minSector;
    FtlTextString unlock;
};
static_assert(sizeof(FtlShipBlueprint) == 592, "ShipBlueprint is the wrong size");

struct FtlDamage {
    int damage;
    int shieldPiercing;
    int fireChance;
    int breachChance;
    int stunChance;
    int ionDamage;
    int systemDamage;
    int persDamage;
    bool hullBuster;
    int ownerId;
    int selfId;
    bool lockdown;
    bool crystalShard;
    bool friendlyFire;
    int stun;
};
static_assert(sizeof(FtlDamage) == 52, "Damage is the wrong size");
