//
// Created by znix on 15/11/2021.
//

#pragma once

#include "ftl_structures.h"

// Not a mistake, this does not extend FtlBlueprint
struct FtlEffectsBlueprint {
    FtlVector<FtlString> launchSounds;
    FtlVector<FtlString> hitShipSounds;
    FtlVector<FtlString> hitShieldsSounds;
    FtlVector<FtlString> missSounds;
    FtlString image;
};
static_assert(sizeof(FtlEffectsBlueprint) == 104, "EffectsBlueprint is the wrong size");

struct FtlWeaponBlueprint : public FtlBlueprint {
    struct MiniProjectile {
        FtlString image;
        bool fake;
    };
    static_assert(sizeof(MiniProjectile) == 16, "MiniProjectile is the wrong size");

    struct BoostPower {
        int type;
        float amount;
        int count;
    };
    static_assert(sizeof(BoostPower) == 12, "BoostPower is the wrong size");

    FtlString typeName;
    FtlDamage damage;
    int shots;
    int missiles;
    float cooldown;
    int power;
    int length;
    float speed;
    int miniCount;
    FtlEffectsBlueprint effects;
    FtlString weaponArt;
    FtlString combatIcon;
    FtlString explosion;
    int radius;
    FtlVector<MiniProjectile> miniProjectiles;
    BoostPower boostPower;
    int droneTargetable;
    int spin;
    int chargeLevels;
    FtlTextString flavourType;
    GL_Colour colour;
};
static_assert(sizeof(FtlWeaponBlueprint) == 424, "WeaponBlueprint is the wrong size");
