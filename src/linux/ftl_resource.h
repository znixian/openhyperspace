//
// Created by znix on 14/11/2021.
//

#pragma once

#include "ftl_map.h"

struct FtlResourceControl {
    struct DynamicImageInfo {
        FtlString name;
        int resid;
        char _pad12[4]; // FIXME is this to be included in the list?
    };

    struct LogicalTexInfo {
        FtlString physName;
        int x, y, w, h;
    };

    FtlUnorderedMap<FtlString, GL_Texture*> images;
    FtlUnorderedMap<int, void> fonts; // TODO value arg should be freetype::font_data
    FtlUnorderedMap<GL_Texture*, void> imageData; // TODO value should be ImageData
    std::pair<int, int> preLoadProgress;
    FtlString nextImageLoaded;
    int imageSwappingMode; // TODO should be an enum
    FtlVector<FtlString> playerShipImages;
    FtlVector<FtlString> hangarShipImages;
    FtlVector<FtlString> enemyShipImages;
    FtlVector<DynamicImageInfo> dynamicImages;
    int dynamicLoadMark;
    FtlUnorderedMap<FtlString, LogicalTexInfo> atlasData;
    void* resmgr; // TODO SIL ResourceManager
    void* package; // TODO SIL? PackageModuleInfo
    GL_Texture* loadingBox;
    GL_Texture* loadingBar;
    int screenWidth, screenHeight;
    int renderPointX, renderPointY;
    GL_FrameBuffer* frameBuffer;
    int fbViewportX, fbViewportY;
    int fbViewportW, fbViewportH;
};
static_assert(sizeof(FtlResourceControl) == 392, "ResourceControl is the wrong size");
