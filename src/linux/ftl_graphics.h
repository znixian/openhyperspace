//
// Created by znix on 13/11/2021.
//

#pragma once

#include "ftl_structures.h"

struct FtlAnimationDescriptor {
    int numFrames;
    int imageWidth, imageHeight;
    int stripStartX, stripStartY;
    int frameWidth, frameHeight;
};
static_assert(sizeof(FtlAnimationDescriptor) == 28, "AnimationDescriptor is the wrong size");

struct FtlAnimationTracker {
    virtual ~FtlAnimationTracker() = default;
    virtual void Update();

    float time = 1;
    bool loop = false;
    float currentTime = 0;
    bool running = false;
    bool reverse = false;
    bool done = true;
    float loopDelay = 0;
    float currentDelay = -1;

    // Helpers copied to reduce hooks
    void SetLoop(bool enableLoop, float delay);
    void Start(float atTime);
};
static_assert(sizeof(FtlAnimationTracker) == 32, "AnimationTracker is the wrong size");

struct FtlAnimation {
    GL_Texture* animationStrip;
    FtlAnimationDescriptor info;
    char _pad36[4];
    FtlAnimationTracker tracker;
    FtlPointf position;
    FtlString soundForwards;
    FtlString soundReverse;
    bool randomiseFrame;
    char _pad97[3];
    float scale;
    float stretchY;
    int currentFrame;
    bool alwaysMirror;
    char _pad113[7];
    FtlVector<FtlVector<FtlString>> soundQueue;
    float fadeOut;
    float startFadeOut;
    FtlString animName;
    int maskPosX, maskSizeX;
    int maskPosY, maskSizeY;
    GL_Primitive* primitive;
    GL_Primitive* mirroredPrimitive;
};
static_assert(sizeof(FtlAnimation) == 192, "Animation is the wrong size");

struct FtlParticle {
    float positionX, positionY;
    float speedX, speedY;
    float accelerationX, accelerationY;
    float lifespan;
    bool alive;
};
static_assert(sizeof(FtlParticle) == 32, "Particle is the wrong size");

struct FtlParticleEmitter {
    FtlParticle particles[64];
    float birthRate;
    float birthCounter;
    float lifespan;
    float speedMag;
    float positionX, positionY;
    float maxDx, minDx;
    float maxDy, minDy;
    int imageX, imageY;
    GL_Primitive* primitive = nullptr;
    float emitAngle;
    bool randAngle;
    bool running;
    float maxAlpha;
    float minSize, maxSize;
    int currentCount;

    ~FtlParticleEmitter()
    {
        fn.CSurface_DestroyPrimitive(primitive);
        primitive = nullptr;
    }
};
static_assert(sizeof(FtlParticleEmitter) == 2128, "ParticleEmitter is the wrong size");

struct FtlTimerHelper {
    int maxTime, minTime;
    int currTime;
    int currGoal;
    bool loop;
    bool running;
};
static_assert(sizeof(FtlTimerHelper) == 20, "TimerHelper is the wrong size");

struct FtlCachedPrimitive {
    virtual ~FtlCachedPrimitive() = 0;

    GL_Primitive* primitive = nullptr;
};
static_assert(sizeof(FtlCachedPrimitive) == 16, "CachedPrimitive is the wrong size");

struct FtlCachedRect : public FtlCachedPrimitive {
    int x, y, w, h;
};
static_assert(sizeof(FtlCachedRect) == 32, "CachedRect is the wrong size");

struct FtlCachedRectOutline : public FtlCachedPrimitive {
    int x, y, w, h;
    int thickness;
};
static_assert(sizeof(FtlCachedRectOutline) == 40, "CachedRectOutline is the wrong size");

struct FtlCachedImage : FtlCachedPrimitive {
    FtlString imageName;
    GL_Texture* texture;
    int x, y;
    float wScale, hScale;
    float xStart, yStart;
    float xSize, ySize;
    float rotation;
    bool mirrored;
};
static_assert(sizeof(FtlCachedImage) == 72, "FtlCachedImage is the wrong size");
