//
// Created by znix on 07/11/2021.
//

#pragma once

#include <bstrwrap.h>
#include <map>

class SymbolSet {
public:
    explicit SymbolSet(const CBString& filename);
    ~SymbolSet();

    std::map<std::string, void*> symbols;

    /**
     * The entry address contained in the ELF header.
     */
    uint64_t entry = 0;

private:
    int64_t mem = 0;
    int64_t mem_len = 0;
};
