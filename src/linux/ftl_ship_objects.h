//
// Created by znix on 13/11/2021.
//

#pragma once

#include "ftl_graphics.h"
#include "ftl_structures.h"

/*
 * Note on vtables:
 *
 * While we're declaring all the virtual functions as pure (the =0 part) they do
 * have definitions. Hence we'll have to implement all of them, and if we want to
 * inherit something we'll have to implement it and call the fn.ClassName_FuncName
 * function from there.
 */

struct FtlSlot {
    int roomId;
    int slotId;
    FtlPoint worldLocation;
};
static_assert(sizeof(FtlSlot) == 16, "Slot is the wrong size");

struct FtlShipObject {
    // Implicit vtable
    int shipId;

    virtual ~FtlShipObject() = default; // TODO figure out how we'll deal with destructors
};
static_assert(sizeof(FtlShipObject) == 16, "ShipObject is the wrong size");

/**
 * An FtlCrewTarget represents an object that a crewmember can target their attacks on. For example, this
 * includes crew and doors.
 */
struct FtlCrewTarget : public FtlShipObject {
    /**
     * @return A point containing the x,y fields rounded to ints
     */
    virtual FtlPoint GetPosition() = 0;
    virtual float PositionShift() = 0;
    virtual bool InsideRoom(int roomId) = 0;

    /**
     * Alter the object's health by a set amount. Negative is damage, positive is healing.
     * @param damage The amount of damage to change by
     * @return True if the target died from this damage, false if still alive or dead before this call
     */
    virtual bool ApplyDamage(float damage) = 0;
    virtual int GetPriority() = 0;
    virtual bool ValidTarget(int shipId) = 0;
    virtual bool MultiShots() = 0;
    virtual bool ExactTarget() = 0;
    virtual bool IsCrew() = 0;
    virtual bool IsCloned() = 0;
    virtual bool IsDrone() = 0;
};
static_assert(sizeof(FtlCrewTarget) == 16, "CrewTarget is the wrong size");
// I assume there's no easy way to assert the vtable size?

struct FtlCrewMember : public FtlCrewTarget {
    float x, y;
    float size;
    float scale;
    float goal_x, goal_y;
    int width, height;
    /**
     * The current and maximum health of the unit. First value is current health, second is max health.
     */
    std::pair<float, float> health;
    float speed_x, speed_y;
    char _pad60[4];
    char _path[48]; // TODO
    bool new_path;
    bool _pad113[3];
    float destination_x, destination_y; // Names are x_destination etc, flipped for ease of use here
    char _pad124[4];
    void* last_door; // TODO FtlDoor
    void* currentRepair; // TODO FtlRepairable
    bool suffocating;
    char _pad145[3];
    int moveGoal;
    int selectionState;
    int roomId;
    int manningId;
    int repairId;
    int stackId;
    FtlSlot currentSlot;
    bool intruder;
    bool fighting;
    bool sharedSpot;
    char _pad191[1];
    FtlCrewAnimation* crewAnim;
    GL_Texture* selectionImage;
    char _healthBox[72]; // TODO FtlCachedImage
    char _healthBoxRed[72]; // TODO FtlCachedImage
    char _healthBar[32]; // TODO FtlCachedRect
    float medbay;
    float lastDamageTimer;
    float lastHealthChange;
    int currentShipId;
    FtlAnimationTracker flashHealthTracker;
    FtlPointf currentTarget;
    void* crewTarget; // TODO FtlCrewTarget
    char _boardingGoal[20]; // TODO FtlBoardingGoal
    bool frozen;
    bool frozenLocation;
    char _pad470[2];
    char _task[12]; // TODO FtlCrewTask
    char _pad484[4];
    FtlString type;
    void* ship; // TODO FtlShip
    char _finalGoal[16]; // TODO FtlSlot
    void* blockingDoor; // TODO FtlDoor
    bool outOfGame;
    char _pad529[7];
    FtlString species;
    bool dead;
    char _pad545[3];
    int onFire;
    bool activeManning;
    char _pad553[7];
    void* currentSystem; // TODO ShipSystem
    int usingSkill;
    char _pad572[4];
    FtlCrewBlueprint blueprint;
    FtlAnimation healing;
    FtlAnimation stunned;
    FtlAnimationTracker levelUp;
    int lastLevelUp;
    char _pad1252[4];
    char _stats[48]; // TODO FtlSCrewStats
    FtlVector<FtlVector<bool>> skillsEarned;
    bool cloneReady;
    bool mindControlled;
    char _pad1330[2];
    int deathNumber;
    FtlAnimation mindControlledAnimation;
    FtlAnimation stunIcon;
    FtlVector<FtlVector<FtlAnimationDescriptor>> skillUp;
    int healthBoost;
    float mindDamageBoost;
    float cloneDying;
    bool resisted;
    char _pad1757[3];
    char _savedPosition[16]; // TODO FtlSlot
    float stunTime;
    char _pad1780[4];
    char _movementTarget[72]; // TODO FtlCachedImage
    bool cloned;
    char _pad1857[7];

    // Constructor
    FtlCrewMember(const FtlCrewBlueprint& bp, int shipId, bool intruder, FtlCrewAnimation* anim);

    // FIXME our destructor will leak memory until we've finished getting all the image etc structs done

    // Functions

    virtual void Jump() = 0;
    virtual bool GetIntruder() = 0;
    virtual void SaveState(int fd) = 0;
    virtual void LoadState(int fd) = 0;
    virtual void OnLoop() = 0;
    virtual void OnRender(bool outlineOnly) = 0;
    virtual bool OutOfGame() = 0;
    virtual void SetOutOfGame() = 0;
    virtual bool Functional() = 0;
    virtual bool CountForVictory() = 0;
    virtual bool GetControllable() = 0;
    virtual bool ReadyToFight() = 0;
    virtual bool CanFight() = 0;
    virtual bool CanRepair() = 0;
    virtual bool CanSabotage() = 0;
    virtual bool CanMan() = 0;
    virtual bool CanTeleport() = 0;
    virtual bool CanHeal() = 0;
    virtual bool CanSuffocate() = 0;
    virtual bool CanBurn() = 0;
    virtual int GetMaxHealth() = 0;
    virtual bool IsDead() = 0;
    virtual bool PermanentDeath() = 0;
    virtual bool ShipDamage(float amount) = 0;
    virtual bool FireFightingSoundEffect() = 0;
    virtual FtlString GetUniqueRepairing() = 0; // Hopefully this handles the output pointer properly
    virtual bool ProvidesVision() = 0;
    virtual float GetMoveSpeedMultiplier() = 0; // Note if you're searching debug symbols: this was spelt 'Multipler'
    virtual float GetRepairSpeed() = 0;
    virtual float GetDamageMultiplier() = 0;
    virtual bool ProvidesPower() = 0;
    virtual FtlString GetSpecies() = 0;
    virtual float GetFireRepairMultiplier() = 0;
    virtual bool IsTelepathic() = 0;
    virtual void GetPowerCooldown() = 0; // TODO fix the return value (a std::pair that goes in xmm0)
    virtual bool PowerReady() = 0;
    virtual void ActivatePower() = 0;
    virtual bool HasSpecialPower() = 0;
    virtual void ResetPower() = 0;
    virtual float GetSuffocationModifier() = 0;
    virtual int BlockRoom() = 0;
    virtual FtlDamage GetRoomDamage() = 0;
    virtual bool IsAnaerobic() = 0;
    virtual void UpdateRepair() = 0;
    virtual bool CanStim() = 0;

    // Convenience functions - there are either mod-specific or copied from the base game to reduce
    // the number of functions we need to hook.
    [[nodiscard]] bool RepairingFire() const;
    [[nodiscard]] bool IsDeadOrDying();
};
static_assert(sizeof(FtlCrewMember) == 1864, "CrewMember is the wrong size");

struct FtlCrewAnimation : public FtlShipObject {
    char _pad12[4];
    FtlVector<FtlVector<FtlAnimation>> animations;
    GL_Texture* baseStrip;
    GL_Texture* colourStrip;
    FtlVector<GL_Texture*> layerStrips;
    FtlPointf lastPosition;
    int direction;
    int subDirection;
    int status;
    int moveDirection;
    FtlParticleEmitter smokeEmitter;
    bool sharedSpot;
    char _pad2233[7];
    FtlVector<char> shots; // TODO FtlCrewLaser - wrong type may break allocation
    FtlTimerHelper shootTimer;
    FtlTimerHelper punchTimer;
    FtlPointf target;
    float damageDone;
    bool player;
    bool frozen;
    bool drone;
    bool ghost;
    bool exactShooting;
    char _pad2321[7];
    FtlAnimation projectile;
    bool typing;
    char _pad2521[7];
    FtlString race;
    int currentShip;
    bool male;
    bool colourBlind;
    char _pad2542[2];
    FtlVector<GL_Colour> layerColours;
    int forcedAnimation;
    int forcedDirection;
    GL_Colour projectileColour;
    bool stunned;
    bool doorTarget;

    // Constructor
    FtlCrewAnimation(int shipId, const FtlString& name, FtlPointf position, bool enemy);

    // I've checked and our destructor should work for this, no need to delegate
};
static_assert(sizeof(FtlCrewAnimation) == 2600, "CrewAnimation is the wrong size");

struct FtlCrewMemberFactory {
    int playerCrew;
    int enemyCrew;
    int enemyCloneCount;
    FtlVector<FtlCrewMember*> crewMembers;
    FtlVector<FtlCrewMember*> lostMembers;
};
static_assert(sizeof(FtlCrewMemberFactory) == 64, "CrewMemberFactory is the wrong size");
