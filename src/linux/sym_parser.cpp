//
// Created by znix on 07/11/2021.
//

#include "sym_parser.h"

#include <elf.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

SymbolSet::SymbolSet(const CBString& filename)
{
    // Read the header table - actually open it properly since it seems this isn't mapped in for whatever reason
    int fd = open(filename, O_RDONLY);
    // TODO check fd
    struct stat st = {};
    int err = fstat(fd, &st);
    // TODO check err
    mem_len = st.st_size;
    mem = (int64_t)mmap(nullptr, mem_len, PROT_READ, MAP_PRIVATE, fd, 0);
    // TODO check if mem == null
    close(fd);

    // Parse the ELF header
    const auto* hdr = (const Elf64_Ehdr*)mem;
    //printf("shoff %lx\n", hdr->e_shoff);

    entry = hdr->e_entry;

    // Parse the section header stringtable
    const auto* headerNamesShdr = (const Elf64_Shdr*)(mem + hdr->e_shoff + hdr->e_shstrndx * hdr->e_shentsize);
    const char* shNameData = (const char*)(mem + headerNamesShdr->sh_offset);

    const Elf64_Shdr* symtab = nullptr;
    const Elf64_Shdr* strtab = nullptr;

    for (int i = 0; i < hdr->e_shnum; i++) {
        const auto* sh = (const Elf64_Shdr*)(mem + hdr->e_shoff + i * hdr->e_shentsize);
        int namePos = sh->sh_name;
        const char* name = shNameData + namePos;
        //printf("section %d: %d '%s'\n", i, name_pos, name);

        if (strcmp(name, ".symtab") == 0)
            symtab = sh;
        if (strcmp(name, ".strtab") == 0)
            strtab = sh;
    }

    if (symtab == nullptr) {
        // TODO proper error message
        abort();
    }
    if (strtab == nullptr) {
        // TODO proper error message
        abort();
    }

    const char* strtabData = (const char*)(mem + strtab->sh_offset);

    int symCount = (int)(symtab->sh_size / sizeof(Elf64_Sym));
    for (int i = 0; i < symCount; i++) {
        auto sym = (const Elf64_Sym*)(mem + symtab->sh_offset + i * sizeof(Elf64_Sym));
        const char* name = strtabData + sym->st_name;

        //printf("sym: %d '%s' %p\n", sym->st_name, name, (void*)sym->st_value);
        symbols[strtabData + sym->st_name] = (void*)sym->st_value;
    }
}

SymbolSet::~SymbolSet()
{
    int err = munmap((void*)mem, mem_len);
    // TODO check err
}
