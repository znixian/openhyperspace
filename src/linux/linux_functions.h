//
// Created by znix on 07/11/2021.
//

#pragma once

#include "common/function_defs.h"
#include "sym_parser.h"

void linux_load_functions(FtlFunctions* functions, SymbolSet* symbols);
