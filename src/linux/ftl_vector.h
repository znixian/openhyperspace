//
// Created by znix on 13/11/2021.
//

#pragma once

#include <stdlib.h>

#include <memory>

/**
 * Implementation of an std::vector that's ABI compatible with FTL.
 *
 * It's likely that whatever vector implementation we link against will be ABI compatible
 * with FTL, but it's better to cut out that bit of chance.
 */
template <typename T, typename Alloc = std::allocator<T>>
class FtlVector {
private:
    // Here's a cool hack: it seems zero-size members do change the layout of the class, so we can
    // avoid letting that mess up our layout by extending it. Do it on an internal class so it doesn't
    // alter the public interface.
    struct Storage : public Alloc {
        T* begin = nullptr;
        T* end = nullptr;
        T* capacity = nullptr;
    } store;

public:
    FtlVector() = default;
    ~FtlVector()
    {
        if (store.begin) {
            store.deallocate(store.begin, capacity());
            store.begin = nullptr;
            store.end = nullptr;
            store.capacity = nullptr;
        }
    }

    FtlVector(std::initializer_list<T> init)
    {
        set_capacity(init.size());
        // AFAIK I did the rvalue thing right?
        for (T value : std::move(init)) {
            push_back(std::move(value));
        }
    }

    FtlVector(const FtlVector& v)
        : FtlVector()
    {
        *this = v;
    }

    FtlVector& operator=(FtlVector const& other)
    {
        set_capacity(other.size(), false);
        for (int i = 0; i < size(); i++) {
            new (&store.begin[i]) T(other[i]);
        }
        store.end = store.begin + other.size();
        return *this;
    }

    [[nodiscard]] inline int size() const { return store.end - store.begin; }
    [[nodiscard]] inline bool empty() const { return size() == 0; }
    [[nodiscard]] inline int capacity() const { return store.capacity - store.begin; }
    [[nodiscard]] inline bool is_index_valid(int idx) const { return 0 <= idx && idx < size(); }

    [[nodiscard]] inline const T& operator[](int idx) const
    {
        if (!is_index_valid(idx))
            abort();
        return store.begin[idx];
    }

    [[nodiscard]] inline T& operator[](int idx)
    {
        if (!is_index_valid(idx))
            abort();
        return store.begin[idx];
    }

    void expand_to_fit(int new_cap)
    {
        int current_cap = capacity();
        if (current_cap > new_cap)
            return;

        if (new_cap < 10)
            new_cap = 10;
        if (new_cap < current_cap * 2)
            new_cap = current_cap * 2;

        set_capacity(new_cap);

        if (capacity() < new_cap)
            abort();
    }

    void set_capacity(int count, bool preserve_previous = true)
    {
        int to_copy = std::min(count, size());

        T* new_storage = store.allocate(count);
        if (preserve_previous) {
            for (int i = 0; i < to_copy; i++) {
                new (&new_storage[i]) T(std::move(store.begin[i]));
            }
        }

        for (T* i = store.begin; i != store.end; i++)
            i->~T();
        store.deallocate(store.begin, capacity());

        store.begin = new_storage;
        store.end = new_storage + to_copy;
        store.capacity = new_storage + count;
    }

    void push_back(T& instance)
    {
        expand_to_fit(size() + 1);
        new (store.end) T(instance);
        store.end++;
    }

    void push_back(T&& instance)
    {
        expand_to_fit(size() + 1);
        new (store.end) T(std::move(instance));
        store.end++;
    }
};
static_assert(sizeof(FtlVector<int>) == 24, "FtlVector is the wrong size");
