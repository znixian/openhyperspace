//
// Created by znix on 07/11/2021.
//

#pragma once

#include <GL/gl.h>

// Not actually an FTL-defined string - the platform code will define this as extending the
// correct string type.
// TODO make this platform-independent
struct OldString;
typedef OldString FtlString;
#include "linux/ftl_vector.h"

struct FtlCApp;
struct FtlCFPS;
struct FtlResourceControl;
struct FtlMainMenu;
struct FtlWorldManager;
struct FtlBlueprintManager;
struct FtlCrewAI;
struct FtlCombatAI;
struct FtlShipAI;
struct FtlCompleteShip;
struct FtlBossShip;
struct FtlLocation;
struct FtlLocationEvent;
struct FtlDamage;
struct FtlEquipment;
struct FtlProjectileFactory;
struct FtlProjectile;
struct FtlAugmentEquipBox;
struct FtlWeaponMount;
struct FtlShipAchivementInfo;
struct FtlSpaceManager;

struct FtlTextString;
struct FtlDescription;
struct FtlBlueprint;
struct FtlCrewBlueprint;
struct FtlWeaponBlueprint;
struct FtlDroneBlueprint;
struct FtlAugmentBlueprint;
struct FtlSystemBlueprint;
struct FtlShipBlueprint;

// GUI
struct FtlFocusWindow;
struct FtlWindowFrame;
struct FtlResourceEvent;
struct FtlGenericButton;
struct FtlChoiceText;
struct FtlChoiceBox;
struct FtlSlideBar;
struct FtlButton;
struct FtlControlButton;
struct FtlTextButton;
struct FtlTextInput;
struct FtlDropBox;
struct FtlEquipmentBox;
struct FtlCrewEquipBox;
struct FtlStarMap;
struct FtlWarningMessage;
struct FtlConfirmWindow;
struct FtlInfoBox;
struct FtlStoreBox;
struct FtlStore;
struct FtlMenuScreen;
struct FtlCreditScreen;
struct FtlGameOver;
struct FtlOptionsScreen;
struct FtlInputBox;
struct FtlTabbedWindow;
struct FtlUpgradeBox;
struct FtlReactorButton;
struct FtlUpgrades;
struct FtlCrewManifest;
struct FtlCommandGui;

// Graphics
struct GL_Colour;
struct FtlPoint;
struct FtlPointf;
struct FtlRect;
struct FtlAnimation;
struct FtlAnimationDescriptor;
struct FtlAnimationTracker;
struct FtlParticle;
struct FtlParticleEmitter;
struct FtlTimerHelper;
struct FtlCachedPrimitive;
struct FtlCachedRect;
struct FtlCachedRectOutline;
struct FtlCachedImage;

// Ship objects and crew
struct FtlCrewAnimation;
struct FtlSlot;
struct FtlShipObject;
struct FtlCrewTarget;
struct FtlCrewMember;
struct FtlCrewMemberFactory;
struct FtlTargetable;
struct FtlCollideable;
struct FtlSelectable;
struct FtlRepairable;
struct FtlRoom; // TODO move
struct FtlDoor; // TODO move
struct FtlOuterHull; // TODO move
struct FtlShip; // TODO move
struct FtlShipManager; // TODO move
struct FtlDamageMessage; // TODO move
struct FtlDrone; // TODO move
struct FtlSpaceDrone; // TODO move

// Systems
struct FtlShipSystem;
struct FtlOxygenSystem;
struct FtlTeleportSystem;
struct FtlCloakingSystem;
struct FtlBatterySystem;
struct FtlMindSystem;
struct FtlCloneSystem;
struct FtlHackingSystem;
struct FtlShields;
struct FtlWeaponSystem;
struct FtlDroneSystem;
struct FtlEngineSystem;
struct FtlMedbaySystem;
struct FtlArtillerySystem;

struct GL_Texture;
struct GL_Primitive;
typedef int GL_FrameBuffer;

typedef void (*FtlCApp_OnExecute)(FtlCApp* this_);

typedef float (*FtlCFPS_GetSpeedFactor)(FtlCFPS* this_);

typedef void (*FtlResourceControl_RenderImage_GL)(FtlResourceControl* this_, GL_Texture*, int, int, int, /* GL_Colour <- is this right? */ float, bool);
typedef char* (*FtlResourceControl_LoadFile)(FtlResourceControl* this_, const FtlString& name);

typedef void (*FtlCSurface_DisableBlend)();
typedef void (*FtlCSurface_EnableBlend)();
typedef void (*FtlCSurface_DestroyPrimitive)(GL_Primitive* primitive);

typedef void (*FtlMainMenu_OnRender)(FtlMainMenu* this_);
typedef void (*FtlMainMenu_Close)(FtlMainMenu* this_);

typedef void (*FtlShipBuilder_Open)(void* this_);

typedef bool (*FtlShip_Import)(FtlShip* this_, const FtlString& name);

typedef FtlCommandGui* (*FtlWorldManager_CreateCommandGui)(FtlWorldManager* this_);
typedef void (*FtlWorldManager_StartGame)(FtlWorldManager* this_, FtlShipManager* ship);
typedef void (*FtlWorldManager_OnLoop)(FtlWorldManager* this_);

typedef void (*FtlShipManager_Ctor)(FtlShipManager* this_, int shipId);
typedef void (*FtlShipManager_OnInit)(FtlShipManager* this_, const FtlShipBlueprint* blueprint, int level);
typedef void (*FtlShipManager_UpdateEnvironment)(FtlShipManager* this_);
typedef FtlCrewMember* (*FtlShipManager_AddCrewMember_ByBlueprint)(FtlShipManager* this_, FtlCrewBlueprint* bp, int slot, bool init, int roomId, bool intruder);
typedef FtlCrewMember* (*FtlShipManager_AddCrewMember_ByString)(FtlShipManager* this_, FtlString* name, FtlString* type, bool intruder, int roomId, bool init, bool male);
typedef void (*FtlShipManager_AddCrewMember_ByObject)(FtlCompleteShip* this_, FtlCrewMember* crewMember, int roomId);

typedef void (*FtlEquipment_AddWeapon)(FtlEquipment* this_, const FtlWeaponBlueprint* weapon, bool free, bool forceCargo);

typedef FtlCrewBlueprint* (*FtlBlueprintManager_GetCrewBlueprint)(FtlCrewBlueprint* out, FtlBlueprintManager* this_, const FtlString& name);
typedef const FtlWeaponBlueprint* (*FtlBlueprintManager_GetWeaponBlueprint)(FtlBlueprintManager* this_, const FtlString& name);
typedef const FtlShipBlueprint* (*FtlBlueprintManager_GetShipBlueprint)(FtlBlueprintManager* this_, const FtlString& name, int sector);

typedef FtlCrewMember* (*FtlCompleteShip_AddCrewMember_ByBlueprint)(FtlCompleteShip* this_, FtlCrewBlueprint* crew, bool intruder);
typedef FtlCrewMember* (*FtlCompleteShip_AddCrewMember_ByString)(FtlCompleteShip* this_, const FtlString& type, FtlString* name, bool intruder);
typedef void (*FtlCompleteShip_AddCrewMember_ByObject)(FtlCompleteShip* this_, FtlCrewMember* crew, int roomId);

typedef bool (*FtlShipObject_HasEquipment)(FtlShipObject* this_, FtlString* name);
typedef float (*FtlShipObject_GetAugmentationValue)(FtlShipObject* this_, FtlString* name);

typedef bool (*FtlCrewMember_Ctor)(FtlCrewMember* this_, const FtlCrewBlueprint& myBlueprint, int shipId, bool intruder, FtlCrewAnimation* anim);
typedef bool (*FtlCrewMember_DirectModifyHealth)(FtlCrewMember* this_, float delta);
typedef void (*FtlCrewMember_SaveState)(FtlCrewMember* this_, int fd);
typedef void (*FtlCrewMember_LoadState)(FtlCrewMember* this_, int fd);
typedef void (*FtlCrewMember_OnLoop)(FtlCrewMember* this_);
typedef void (*FtlCrewMember_OnRender)(FtlCrewMember* this_, bool outlineOnly);
typedef void (*FtlCrewMember_SetOutOfGame)(FtlCrewMember* this_);
typedef bool (*FtlCrewMember_ShipDamage)(FtlCrewMember* this_, float delta);
typedef void (*FtlCrewMember_UpdateRepair)(FtlCrewMember* this_);
typedef void (*FtlCrewMember_UpdateHealth)(FtlCrewMember* this_);

typedef FtlCrewMember* (*FtlCrewMemberFactory_CreateCrewmember)(FtlCrewMemberFactory* this_, const FtlCrewBlueprint* blueprint, int shipId, bool intruder);

typedef void (*FtlCrewAnimation_Ctor)(FtlCrewAnimation* this_, int shipId, FtlString name, FtlPointf position, bool enemy);

typedef void (*FtlAnimationTracker_Update)(FtlAnimationTracker* this_);

typedef void (*FtlGenericButton_MouseMove)(FtlGenericButton* this_, int mX, int mY, bool silent);
typedef void (*FtlTextButton_OnRender)(FtlGenericButton* this_);
typedef void (*FtlTextButton_ResetPrimitives)(FtlGenericButton* this_);
typedef int (*FtlTextButton_GetIdealButtonWidth)(FtlGenericButton* this_);

typedef void (*FtlOxygenSystem_ComputeAirLoss)(FtlOxygenSystem* this_, int roomId, float amount, bool silent);

typedef void (*FtlRoom_ctor)(FtlRoom* this_, int shipId, int x, int y, int w, int h, int roomId);

struct FtlFunctions {
    // Class CApp
    FtlCApp_OnExecute CApp_OnExecute;

    // Class CFPS
    FtlCFPS_GetSpeedFactor CFPS_GetSpeedFactor;
    FtlCFPS* CFPS_FPSControl;

    // Class ResourceControl
    FtlResourceControl_RenderImage_GL ResourceControl_RenderImage_GL;
    FtlResourceControl_LoadFile ResourceControl_LoadFile;

    FtlResourceControl* ResourceControl_GlobalResources;

    // Class CSurface
    FtlCSurface_DisableBlend CSurface_DisableBlend;
    FtlCSurface_EnableBlend CSurface_EnableBlend;
    FtlCSurface_DestroyPrimitive CSurface_DestroyPrimitive;

    // Class MainMenu
    FtlMainMenu_OnRender MainMenu_OnRender;
    FtlMainMenu_Close MainMenu_Close;

    // Class ShipBuilder
    FtlShipBuilder_Open ShipBuilder_Open;

    // Class Ship
    FtlShip_Import Ship_Import;

    // Class BlueprintManager
    FtlBlueprintManager* BlueprintManager_Instance;
    FtlBlueprintManager_GetCrewBlueprint BlueprintManager_GetCrewBlueprint;
    FtlBlueprintManager_GetWeaponBlueprint BlueprintManager_GetWeaponBlueprint;
    FtlBlueprintManager_GetShipBlueprint BlueprintManager_GetShipBlueprint;

    // Class WorldManager
    FtlWorldManager_CreateCommandGui WorldManager_CreateCommandGui;
    FtlWorldManager_StartGame WorldManager_StartGame;
    FtlWorldManager_OnLoop WorldManager_OnLoop;

    // Class ShipManager
    FtlShipManager_Ctor ShipManager_Ctor;
    FtlShipManager_OnInit ShipManager_OnInit;
    FtlShipManager_UpdateEnvironment ShipManager_UpdateEnvironment;
    FtlShipManager_AddCrewMember_ByBlueprint ShipManager_AddCrewMember_ByBlueprint;
    FtlShipManager_AddCrewMember_ByString ShipManager_AddCrewMember_ByString;
    FtlShipManager_AddCrewMember_ByObject ShipManager_AddCrewMember_ByObject;

    // Class Equipment
    FtlEquipment_AddWeapon Equipment_AddWeapon;

    // Class CompleteShip
    FtlCompleteShip_AddCrewMember_ByBlueprint CompleteShip_AddCrewMember_ByBlueprint;
    FtlCompleteShip_AddCrewMember_ByString CompleteShip_AddCrewMember_ByString;
    FtlCompleteShip_AddCrewMember_ByObject CompleteShip_AddCrewMember_ByObject;

    // Class ShipObject
    FtlShipObject_HasEquipment ShipObject_HasEquipment;
    FtlShipObject_GetAugmentationValue ShipObject_GetAugmentationValue;

    // Class CrewMember
    FtlCrewMember_Ctor CrewMember_Ctor;
    FtlCrewMember_DirectModifyHealth CrewMember_DirectModifyHealth;
    FtlCrewMember_SaveState CrewMember_SaveState;
    FtlCrewMember_LoadState CrewMember_LoadState;
    FtlCrewMember_OnLoop CrewMember_OnLoop;
    FtlCrewMember_OnRender CrewMember_OnRender;
    FtlCrewMember_SetOutOfGame CrewMember_SetOutOfGame;
    FtlCrewMember_ShipDamage CrewMember_ShipDamage;
    FtlCrewMember_UpdateRepair CrewMember_UpdateRepair;
    FtlCrewMember_UpdateHealth CrewMember_UpdateHealth;

    FtlCrewMemberFactory* CrewMemberFactory_Instance;
    FtlCrewMemberFactory_CreateCrewmember CrewMemberFactory_CreateCrewmember;

    FtlCrewAnimation_Ctor CrewAnimation_Ctor;
    FtlAnimationTracker_Update AnimationTracker_Update;

    FtlGenericButton_MouseMove GenericButton_MouseMove;
    FtlTextButton_OnRender TextButton_OnRender;
    FtlTextButton_ResetPrimitives TextButton_ResetPrimitives;
    FtlTextButton_GetIdealButtonWidth TextButton_GetIdealButtonWidth;

    FtlRoom_ctor Room_Ctor;

    // /////////
    // Blueprints etc
    // /////////

    // /////////
    // Systems
    // /////////
    FtlOxygenSystem_ComputeAirLoss OxygenSystem_ComputeAirLoss;

    // /////////
    // Platform-dependant stuff
    // /////////

    void* linux_empty_str_ptr; // See old_string.cpp
};

// Global functions variable
extern FtlFunctions fn;

// Global app instance - this isn't really the right place to put it, but it'll do
extern FtlCApp* ftlApp;
