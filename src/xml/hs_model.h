//
// Created by znix on 14/11/2021.
//

#pragma once

#include <map>
#include <memory>
#include <string>
#include <vector>

#include "linux/ftl_structures.h"

#include <rapidxml.hpp>

class CrewTypeDefinition;

/**
 * A memory representation of the hyperspace.xml file's contents
 */
class HsModel {
public:
    explicit HsModel(const std::string& contents);

    [[nodiscard]] inline const CrewTypeDefinition* GetCrewType(const std::string& name) const
    {
        auto iter = races.find(name);
        if (iter == races.end())
            return nullptr;
        else
            return iter->second.get();
    }

private:
    void XmlError(rapidxml::xml_node<>* node, const char* msg, ...);
    int ParseInt(rapidxml::xml_node<>* node, const char* value, const char* name, int defaultValue = -1);
    float ParseFloat(rapidxml::xml_node<>* node, const char* value, const char* name, float defaultValue = 1);
    bool ParseBool(rapidxml::xml_node<>* node, const char* value, const char* name, bool defaultValue = false);

    void ParseCrew(rapidxml::xml_node<>* node);
    void ParseRace(rapidxml::xml_node<>* node);

private:
    // A mutable copy of the XML text stored for use by RapidXML
    std::vector<char> mutableContents;

    // The offsets in the file where each newline appears. This is used for error messages, and is
    // cleared when parsing is complete.
    std::vector<int> lineEndingPositions;

    std::map<std::string, std::unique_ptr<CrewTypeDefinition>> races;
};

class CrewTypeDefinition {
public: // Functions
    [[nodiscard]] FtlCrewBlueprint FindBlueprint() const;

public: // Fields
    std::string raceName;

    // Set of how many points the crew must get to level up fully
    struct SkillRequirements {
        int piloting = -1;
        int engines = -1;
        int shields = -1;
        int weapons = -1;
        int repair = -1;
        int combat = -1;
    } skills;

    float repairSpeed = 1;
    float damageMultiplier = 1;
    float moveSpeedMultiplier = 1;
    float fireRepairMultiplier = 1;
    float suffocationModifier = 1;
    float fireDamageMultiplier = 1;
    float damageTakenMultiplier = 1;
    float sabotageSpeedMultiplier = 1;
    float stunMultiplier = 1;

    int maxHealth = 100;
    int defaultSkillLevel = 0;

    bool controllable = true;

    bool canSuffocate = true;
    bool canBurn = true;
    bool canPhaseThroughDoors = false;
    bool canFight = true;
    bool canRepair = true;
    bool canSabotage = true;
    bool canMan = true;

    float oxygenChangeSpeed = 0;
    bool isTelepathic = false;
    bool isAnaerobic = false; // Note this directly drains oxygen, normally use oxygenChangeSpeed instead
    int bonusPower = 0;
    int powerDrain = 0;
    bool powerDrainFriendly = false;
    float damageEnemiesAmount = 0;
    bool cloneLoseSkills = true;
    bool detectsLifeforms = false;

    float healSpeed = 1;
    float passiveHealAmount = 0;
    float trueHealAmount = 0;
    float healCrewAmount = 0;

    std::vector<std::string> deathSounds;
    std::vector<std::string> shootingSounds;
    std::vector<std::string> repairSounds;

    // TODO these fields:
    // animBase
    // passiveHealDelay - TODO unit?
    // nameRace
    // deathEffect
    // powerEffect
    // droneAI
    // droneMoveFromManningSlot
};
