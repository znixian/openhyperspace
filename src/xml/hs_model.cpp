//
// Created by znix on 14/11/2021.
//

#include "hs_model.h"

#include <rapidxml.hpp>

#include <stdarg.h>
#include <string.h>

using namespace rapidxml;

HsModel::HsModel(const std::string& contents)
{
    mutableContents.insert(mutableContents.begin(), contents.begin(), contents.end());
    mutableContents.push_back(0); // Make sure it's null-terminated

    // Build the line ending table
    for (size_t i = 0; i < mutableContents.size(); i++) {
        if (mutableContents.at(i) == '\n') {
            lineEndingPositions.push_back(i);
        }
    }

    xml_document<> doc;
    doc.parse<0>(mutableContents.data()); // Note this requires a non-const char* reference

    xml_node<>* root = doc.first_node("FTL");
    printf("root node: %s\n", root->name());

    for (xml_node<>* node = root->first_node(); node != nullptr; node = node->next_sibling()) {
        std::string name = node->name();

        if (name == "crew") {
            ParseCrew(node);
        } else {
            printf("found not-yet-supported node %s\n", node->name());
        }
    }

    // TODO these nodes:
    // version
    // saveFile
    // infinite
    // discord
    // hullNumbers
    // hackingDroneFix
    // advancedCrewTooltips
    // eventTooltips
    // showNumericalWeaponCooldown
    // redesignedWeaponTooltips
    // titleScreen
    // forceDlc
    // seeds
    // console
    // ships
    // crew
    // drones
    // augments
    // events
    // boss
    // store

    // Don't need these anymore
    lineEndingPositions.clear();
}

void HsModel::XmlError(xml_node<>* node, const char* msg, ...)
{
    va_list args;
    char* res = nullptr;

    va_start(args, msg);
    vasprintf(&res, msg, args);
    va_end(args);

    int namePos = (int)(node->name() - mutableContents.data());

    int line = 0;
    int lineStart = 0;
    for (; line < (int)lineEndingPositions.size(); line++) {
        int thisLineStart = lineEndingPositions.at(line);
        if (thisLineStart > namePos)
            break;

        // When we break, this will have the position of the last line's newline.
        // Note the +1 jumps the newline itself
        // If there's a CR there too we'll be off by one, but it'll be close enough.
        lineStart = thisLineStart + 1;
    }

    fprintf(stderr, "Error parsing hyperspace XML node '%s' at %d:%d due to error: %s\n", node->name(), line + 1, namePos - lineStart, res);

    free(res);
}

int HsModel::ParseInt(xml_node<>* node, const char* value, const char* name, int defaultValue)
{
    char* end = nullptr;
    int parsed = (int)std::strtol(value, &end, 10);
    if (*end) {
        XmlError(node, "Failed to parse %s value '%s' as int, at char '%c'", name, value, *end);
        return defaultValue;
    }

    return parsed;
}

float HsModel::ParseFloat(xml_node<>* node, const char* value, const char* name, float defaultValue)
{
    char* end = nullptr;
    float parsed = std::strtof(value, &end);
    if (*end) {
        XmlError(node, "Failed to parse %s value '%s' as float, at char '%c'", name, value, *end);
        return defaultValue;
    }

    return parsed;
}

bool HsModel::ParseBool(xml_node<>* node, const char* value, const char* name, bool defaultValue)
{
    if (strcmp(value, "true") == 0) {
        return true;
    } else if (strcmp(value, "false") == 0) {
        return false;
    } else {
        XmlError(node, "Failed to parse %s value '%s' as bool, should be 'true' or 'false'", name, value);
        return defaultValue;
    }
}

void HsModel::ParseCrew(rapidxml::xml_node<>* root)
{
    for (xml_node<>* node = root->first_node("race"); node != nullptr; node = node->next_sibling("race")) {
        ParseRace(node);
    }
}

void HsModel::ParseRace(xml_node<>* node)
{
    CrewTypeDefinition type;

    xml_attribute<>* nameAttr = node->first_attribute("name");

    if (nameAttr == nullptr) {
        XmlError(node, "name attribute not set");
        return;
    }
    std::string raceName = nameAttr->value();
    type.raceName = raceName;

    if (races.count(raceName)) {
        XmlError(node, "Duplicate race name '%s'", raceName.c_str());
        return;
    }

    xml_node<>* skillsNode = node->first_node("skills");
    if (skillsNode != nullptr) {
        auto parse = [this, skillsNode](const char* name) {
            xml_node<>* n = skillsNode->first_node(name);
            if (n == nullptr)
                return -1;
            xml_attribute<>* attr = n->first_attribute("req");
            if (attr == nullptr) {
                XmlError(n, "Found skill type element without 'req' attribute");
                return -1;
            }
            return ParseInt(n, attr->value(), "req");
        };

        type.skills.piloting = parse("piloting");
        type.skills.engines = parse("engines");
        type.skills.shields = parse("shields");
        type.skills.weapons = parse("weapons");
        type.skills.repair = parse("repair");
        type.skills.combat = parse("combat");
    }

#define LOAD_VALUE(name, parser)                                                   \
    do {                                                                           \
        xml_node<>* name##Node = node->first_node(#name);                          \
        if (name##Node) {                                                          \
            type.name = parser(name##Node, name##Node->value(), #name, type.name); \
        }                                                                          \
    } while (0)

#define LOAD_FLOAT(name) LOAD_VALUE(name, ParseFloat)
#define LOAD_INT(name) LOAD_VALUE(name, ParseInt)
#define LOAD_BOOL(name) LOAD_VALUE(name, ParseBool)

    LOAD_FLOAT(repairSpeed);
    LOAD_FLOAT(damageMultiplier);
    LOAD_FLOAT(moveSpeedMultiplier);
    LOAD_FLOAT(fireRepairMultiplier);
    LOAD_FLOAT(suffocationModifier);
    LOAD_FLOAT(fireDamageMultiplier);
    LOAD_FLOAT(damageTakenMultiplier);
    LOAD_FLOAT(sabotageSpeedMultiplier);
    LOAD_FLOAT(stunMultiplier);

    LOAD_INT(maxHealth);
    LOAD_INT(defaultSkillLevel);

    LOAD_BOOL(controllable);

    LOAD_BOOL(canSuffocate);
    LOAD_BOOL(canBurn);
    LOAD_BOOL(canPhaseThroughDoors);
    LOAD_BOOL(canFight);
    LOAD_BOOL(canRepair);
    LOAD_BOOL(canSabotage);
    LOAD_BOOL(canMan);

    LOAD_FLOAT(oxygenChangeSpeed);
    LOAD_BOOL(isTelepathic);
    LOAD_BOOL(isAnaerobic);
    LOAD_INT(bonusPower);
    LOAD_INT(powerDrain);
    LOAD_BOOL(powerDrainFriendly);
    LOAD_FLOAT(damageEnemiesAmount);
    LOAD_BOOL(cloneLoseSkills);
    LOAD_BOOL(detectsLifeforms);

    LOAD_FLOAT(healSpeed);
    LOAD_FLOAT(passiveHealAmount);
    LOAD_FLOAT(trueHealAmount);
    LOAD_FLOAT(healCrewAmount);

    // TODO read deathSounds, shootingSounds, repairSounds

    std::unique_ptr<CrewTypeDefinition> typePtr = std::make_unique<CrewTypeDefinition>();
    *typePtr = std::move(type);
    races[raceName] = std::move(typePtr);
}

FtlCrewBlueprint CrewTypeDefinition::FindBlueprint() const
{
    FtlCrewBlueprint bp;
    fn.BlueprintManager_GetCrewBlueprint(&bp, fn.BlueprintManager_Instance, raceName);
    return bp;
}
