//
// Created by znix on 04/12/2021.
//

#pragma once

/**
 * A collection of hooks the test system needs run.
 */
struct TestHooks {
    /// The entry address of the main binary, since this has already been found
    /// This is read by the test system, unlike all the other fields
    void* entry = nullptr;

    void (*onStartup)() = nullptr;
    void (*onUpdate)() = nullptr;
};

typedef void (*load_test_system_func)(TestHooks* hooks);
